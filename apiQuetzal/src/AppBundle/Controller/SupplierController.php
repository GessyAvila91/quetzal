<?php
/**
 * Created by PhpStorm.
 * User: GessyAvila
 * Date: 28-Jul-17
 * Time: 6:45 AM
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;

use BackendBundle\Entity\Supplier;

class SupplierController extends  Controller{

    public function indexAction(Request $request, $id = null) {
        //echo("ProveedorController2");
        $helpers = $this->get("app.helpers");

        $em = $this->getDoctrine()->getManager();

        $solicitud = $em->getRepository('BackendBundle:Supplier')->findAll();

        return $helpers->json($solicitud);
    }

    public function searchAction(Request $request, $search = null){
        //echo("SupplierSearchController2");
        $helpers = $this->get("app.helpers");

        $em = $this->getDoctrine()->getManager();

        if($search != null){
            $dql = "SELECT p FROM BackendBundle:Supplier p "
                . "WHERE p.id LIKE '%$search%' OR "
                . "p.razonsocial LIKE '%$search%' ORDER BY p.id ASC";
        }else{
            $dql = "SELECT p FROM BackendBundle:Supplier p ORDER BY p.id ASC";
        }

        $query = $em->createQuery($dql);

        $page = $request->query->getInt("page", 1);
        $paginator = $this->get("knp_paginator");
        $items_per_page = 10;

        $pagination = $paginator->paginate($query, $page, $items_per_page);
        $total_items_count = $pagination->getTotalItemCount();

        $data = array(
            "status" => "success",
            "total_items_count" => $total_items_count,
            "page_actual" => $page,
            "items_per_page" => $items_per_page,
            "total_pages" => ceil($total_items_count / $items_per_page),
            "data" => $pagination
        );

        return $helpers->json($data);
    }

    public function newAction(Request $request) {

    }

    public function editAction(Request $request) {
        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);

        if ($json != null) {
            $params = json_decode($json);

            $id = (isset($params->id)) ? $params->id: null;

            $tradename    = (isset($params->tradename)) ? $params->tradename: null;
            $businessname = (isset($params->businessname)) ? $params->businessname: null;
            $addres       = (isset($params->addres)) ? $params->addres: null;
            $phone        = (isset($params->phone)) ? $params->phone: null;
            $email        = (isset($params->email)) ? $params->email : null;
            $rfc          = (isset($params->rfc)) ? $params->rfc : null;
            $description  = (isset($params->description)) ? $params->description: null;
            $webpage    = (isset($params->webpage)) ? $params->webpage: null;
            $contactperson    = (isset($params->contactperson)) ? $params->contactperson: null;

            if ($tradename != null) {
                $em = $this->getDoctrine()->getManager();
                $supplier= $em->getRepository("BackendBundle:Supplier")->findOneBy(array(
                        "id" => $id)
                );

                $supplier->setTradename($tradename);
                $supplier->setBusinessname($businessname);
                $supplier->setAddres($addres);
                $supplier->setPhone($phone);
                $supplier->setEmail($email);

                $supplier->setWebpage($webpage);
                $supplier->setRfc($rfc);
                $supplier->setDescription($description);
                $supplier->setWebpage($webpage);
                $supplier->setContactperson($contactperson);

                $em = $this->getDoctrine()->getManager();

                $em->persist($supplier);
                $em->flush();

                $data = array(
                    "status" => "success",
                    "code" => 200,
                    "msg" => "Supplier updated success!!"
                );

            } else {
                $data = array(
                    "status" => "error",
                    "code" => 400,
                    "msg" => "Supplier updated  error"
                );
            }
        } else {
            $data = array(
                "status" => "error",
                "code" => 400,
                "msg" => "Supplier not updated , params failed"
            );
        }
        return $helpers->json($data);

    }
}