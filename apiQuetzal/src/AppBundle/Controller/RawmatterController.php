<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;

use BackendBundle\Entity\Rawmatter;

class RawmatterController extends Controller{
    //MARK: DEMO functions
    public function indexAction(Request $request){
        $helpers = $this->get("app.helpers");

        $em = $this->getDoctrine()->getManager();
        $solicitud = $em->getRepository('BackendBundle:Rawmatter')->findAll();

        return $helpers->jsonRawmatter($solicitud);
    }

    public function gettestAction(Request $request){
        die();
        $helpers = $this->get("app.helpers");

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('BackendBundle:Usuario')->findAll();

        return $helpers->json($users);
    }

    public function posttestAction(Request $request){
        echo("POST TEST");
        die();
        $helpers = $this->get("app.helpers");

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('BackendBundle:Usuario')->findAll();

        return $helpers->json($users);
    }

    //MARK:

    public function newAction(Request $request){
        $helpers = $this->get("app.helpers");

        $json = $request->get("json", null);
        $params = json_decode($json);

        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "RawMatter not created"
        );

        if ($json != null) {

            $code        = (isset($params->code))        ? $params->code: null;
            $name        = (isset($params->name))        ? $params->name: null;
            $description = (isset($params->description)) ? $params->description: null;

            $price       = (isset($params->price))       ? $params->price: null;
            $maxStock    = (isset($params->maxStock))    ? $params->maxStock: null;
            $minReorder  = (isset($params->minReorder))  ? $params->minReorder: null;

            if ($code != null || $name != null || $price!= null) {

                if(true){
                    $rawmatter = new Rawmatter();

                    $rawmatter->setCode($code);
                    $rawmatter->setName($name);
                    $rawmatter->setDescription($description);

                    $rawmatter->setPrice($price);
                    $rawmatter->setMinreorder($minReorder);
                    $rawmatter->setMaxstock($maxStock);

                    $em = $this->getDoctrine()->getManager();

                    $em->persist($rawmatter);
                    $em->flush();

                    $data["status"] = 'success';
                    $data["code"] = 200;
                    $data["msg"] = 'New RawMatter created !!';
                }else{
                    $data["status"] = 'error';
                    $data["code"] = 400;
                    $data["msg"] = 'Code Duplicated';
                }

            }else{
                $data["status"] = 'error';
                $data["code"] = 400;
                $data["msg"] = 'Name or Code Null';
            }
        }
        return $helpers->json($data);
    }

    public function searchCodeAction(Request $request){

        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);
        $params = json_decode($json);

        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Search Error"
        );

        if ($json != null) {
            $code = (isset($params->code)) ? $params->code : null;
            if ($code != null) {

                $em = $this->getDoctrine()->getManager();
                $rawmatter = $em->getRepository('BackendBundle:Rawmatter')->findBy(array('code' => $code));

                $data["status"] = 'success';
                $data["code"] = 200;
                $data["msg"] = 'RawMatter found!!';
                $data["data"] = $rawmatter;

            } else {
                $data["status"] = 'error';
                $data["code"] = 400;
                $data["msg"] = 'Code Null';
            }
        }
        return $helpers->json($data);
    }
    public function searchAction(Request $request){
        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);
        $params = json_decode($json);

        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Search Error"
        );
        if ($json != null) {
            $code = (isset($params->code)) ? $params->code : null;
            if ($code != null) {
                $em = $this->getDoctrine()->getManager();
                $dql = ("SELECT r FROM BackendBundle:Rawmatter r WHERE r.name like :code OR r.description LIKE :code");

                $query = $em->createQuery($dql);
                $query->setParameter('code', '%'.$code.'%');
                $info =$query->getResult();
                $data = array(
                    "status" => "success",
                    "code" => "200",
                    "msg" => "found",
                    "data" => $info
                );
            }
        }
        return $helpers->json($data);
    }

    public function updateAction(Request $request){
        $helpers = $this->get("app.helpers");

        $json = $request->get("json", null);
        $params = json_decode($json);

        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "RawMatter not updated"
        );

        if ($json != null) {

            $code        = (isset($params->code))        ? $params->code: null;
            $name        = (isset($params->name))        ? $params->name: null;
            $description = (isset($params->description)) ? $params->description: null;

            $price       = (isset($params->price))       ? $params->price: null;
            $minReorder  = (isset($params->minReorder))  ? $params->minReorder: null;
            $maxStock    = (isset($params->maxStock))    ? $params->maxStock: null;

            if ($code != null || $name != null || $price!= null) {

                if(true){
                    $em = $this->getDoctrine()->getManager();
                    $rawmatter = $em->getRepository('BackendBundle:Rawmatter')->findOneBy(array('code' => $code));

                    $rawmatter->setCode($code);
                    $rawmatter->setName($name);
                    $rawmatter->setDescription($description);

                    $rawmatter->setPrice($price);
                    $rawmatter->setMinreorder($minReorder);
                    $rawmatter->setMaxstock($maxStock);

                    $em = $this->getDoctrine()->getManager();

                    $em->persist($rawmatter);
                    $em->flush();

                    $data["status"] = 'success';
                    $data["code"] = 200;
                    $data["msg"] = 'New RawMatter updated!!';
                }else{
                    $data["status"] = 'error';
                    $data["code"] = 400;
                    $data["msg"] = 'Code Duplicated';
                }

            }else{
                $data["status"] = 'error';
                $data["code"] = 400;
                $data["msg"] = 'Name or Code Null';
            }
        }
        return $helpers->json($data);
    }
}
