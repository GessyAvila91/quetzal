<?php
/**
 * Created by PhpStorm.
 * User: GessyAvila
 * Date: 28-Jul-17
 * Time: 9:23 AM
 */

namespace AppBundle\Controller;

use BackendBundle\Entity\Categorymatter;
use BackendBundle\Entity\Supplier;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\BrowserKit\Response;

use BackendBundle\Entity\Orders;
use BackendBundle\Entity\Orderdetail;

class OrdersController extends Controller{
    //MARK: DEMO functions
    public function indexAction(Request $request){
        $helpers = $this->get("app.helpers");

        $em = $this->getDoctrine()->getManager();
        $solicitud = $em->getRepository('BackendBundle:Orders')->findAll();

        return $helpers->jsonOrder($solicitud);
    }

    public function gettestAction(Request $request){
        $helpers = $this->get("app.helpers");

        $em = $this->getDoctrine()->getManager();
        $solicitud = $em->getRepository('BackendBundle:Orders')->findAll();
        $helpers = $this->get("app.helpers");
        $id = "1";
        $em = $this->getDoctrine()->getManager();

        $dql = "SELECT o FROM BackendBundle:Orders o WHERE o.id = 1 ORDER BY o.id ASC";

        $query = $em->createQuery($dql);

        $info =$query->getResult();
        $data = array(
            "status" => "success",
            "code" => "200",
            "msg" => "found",
            "data" => $info
        );

        return $helpers->jsontest($data);

    }

    public function posttestAction(Request $request){
        $helpers = $this->get("app.helpers");
        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Order not found"
        );
        $em = $this->getDoctrine()->getManager();
        $Order = $em->getRepository('BackendBundle:Order')->findAll();

        return $helpers->json($Order);
    }

    public function getallAction(Request $request){
        $helpers = $this->get("app.helpers");

        $em = $this->getDoctrine()->getManager();
        $orders    = $em->getRepository('BackendBundle:Orders')->findAll();
        $suppliers = $em->getRepository('BackendBundle:Supplier')->findAll();
        $rawmatter = $em->getRepository('BackendBundle:Rawmatter')->findAll();

        $data = array(
            "status" => "success",
            "code" => "200",
            "msg" => "found"
        );

        if(count($orders)>0){
            $data["orders"] = $orders;
        }
        if(count($suppliers)>0){
            $data["supplier"] = $suppliers;
        }
        if(count($rawmatter)>0){
            $data["rawmatter"] = $rawmatter;
        }
        return $helpers->json($data);
    }


    public function newAction(Request $request){
        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);
        $params = json_decode($json);
        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Order not created"
        );
        if ($json != null) {
            $supplierid    = (isset($params->supplierid))  ? $params->supplierid: null;
            $userid        = (isset($params->userid))      ? $params->userid: null;
            $description   = (isset($params->description)) ? $params->description: null;
            $deliverdate   = (isset($params->deliverdate)) ? $params->deliverdate: null;
            $details       = (isset($params->details)) ? $params->details: null;
            //25/04/2015 15:00
            $registerdate   = new \DateTime("now");
            $Deliverdate    =   \DateTime::createFromFormat("d/m/Y H:i",$deliverdate);
            if($registerdate > $Deliverdate){
                $data["status"] = 'error';
                $data["code"] = 400;
                $data["msg"] = 'Data Error';
                $data["Rregisterdate"] = $registerdate;
                $data["Deliverdate"] = $Deliverdate;
                return $helpers->json($data);
            }else{
                if ($supplierid!= null ||(count($details ) < 1) ) {
                    $orders = new Orders();

                    $em = $this->getDoctrine()->getManager();
                    $supplier  = $em->getRepository('BackendBundle:Supplier')->findOneBy(array("id" => $supplierid));
                    $user      = $em->getRepository('BackendBundle:User')->findOneBy(array("id" => $userid));

                    $orders->setSupplierid($supplier);
                    $orders->setUserid($user);
                    $orders->setRegisterdate($registerdate);
                    $orders->setDescription($description);
                    $orders->setDeliverdate($Deliverdate);

                    $em->persist($orders);
                    $em->flush();
                    foreach ($details as &$valor) {
                        $order  = $em->getRepository('BackendBundle:Orders')->findOneBy(array("registerdate" => $registerdate));
                        $rawmatter  = $em->getRepository('BackendBundle:Rawmatter')->findOneBy(array("code" => $valor->rawmattercode));

                        $detail = new Orderdetail();
                        $detail->setOrderid($order);
                        $detail->setRawmattercode($rawmatter);
                        $detail->setQuantity($valor->quantity);
                        $detail->setUnitprice($valor->price);

                        $em->persist($detail);
                        $em->flush();

                        $data["status"] = 'success';
                        $data["code"] = 200;
                        $data["msg"] = 'New Order created!!';
                    }
                }else{
                    $data["status"] = 'error';
                    $data["code"] = 400;
                    $data["msg"] = 'Data Null o detalles 0';
                }
            }
        }
        return $helpers->json($data);
    }
    public function completeAction(Request $request){
        //json={"id":"1","details":[{"code":"011TST","rawmatter":"TST123","seccionid":"1","quantity":10},{"code":"CDS816","rawmatter":"TST234","seccionid":"1","quantity":15}]}
        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);
        $params = json_decode($json);
        $em = $this->getDoctrine()->getManager();
        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Order not Asigned"
        );
        if ($json != null) {
            $id= (isset($params->id)) ? $params->id : null;
            $status= (isset($params->status)) ? $params->status : null;

            $details       = (isset($params->details)) ? $params->details: null;

            if($status != 0){
                $data = array(
                    "status" => "error",
                    "code" => 400,
                    "msg" => "Order Status Error  (!= 0)"
                );
                return $helpers->json($data);
            }
            if (count($details ) > 0) {
                foreach ($details as &$valor) {

                    $CM= $em->getRepository('BackendBundle:Categorymatter')->findOneBy(array(
                        "seccionid" => $valor->seccionid,
                        "categorycode" => $valor->code
                    ));

                    if(count($CM)>0){

                        $x = $CM->getQuantity();
                        $x = $x+($valor->quantity);
                        $CM->setQuantity($x);

                        $em->persist($CM);
                        $em->flush();

                    }else{
                        $category = $em->getRepository('BackendBundle:Category')->findOneBy(array("code" => $valor->code));
                        $seccion  = $em->getRepository('BackendBundle:Seccion')->findOneBy(array("id" => $valor->seccionid));

                        $Categorymatter = new Categorymatter();
                        $Categorymatter->setCategorycode($category);
                        $Categorymatter->setSeccionid($seccion);
                        $Categorymatter->setQuantity($valor->quantity);

                        $em->persist($Categorymatter);
                        $em->flush();
                    }

                    $data["status"] = 'success';
                    $data["code"] = 200;
                    $data["msg"] = 'Order Asign!!';
                }
                $order     = $em->getRepository('BackendBundle:Orders')->findOneBy(array("id" => $id));
                $order->setStatus(1);
                $em->persist($order);
                $em->flush();
            }

        }else{
            //echo("true");
            $data = array(
                "status" => "error",
                "code" => 400,
                "msg" => "Order No Asign"
            );
        }
        return $helpers->json($data);
    }
    public  function statusAction(Request $request){
        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);
        $params = json_decode($json);
        $em = $this->getDoctrine()->getManager();
        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Order not updated"
        );
        if ($json != null) {
            $id     = (isset($params->id)) ? $params->id : null;
            $status = (isset($params->status)) ? $params->status: null;
            $order = $em->getRepository('BackendBundle:Orders')->findOneBy(array("id" => $id));

            $order->setStatus($status);

            $em->persist($order);
            $em->flush();
            $data["status"] = 'success';
            $data["code"] = 200;
            $data["msg"] = 'Order status updated!!';
        }
        return $helpers->jsonOrder($data);
    }
    public  function updateAction(Request $request){
        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);
        $params = json_decode($json);
        $em = $this->getDoctrine()->getManager();
        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Order not updated"
        );
        if ($json != null) {
            $id= (isset($params->id)) ? $params->id : null;
            $quantity= (isset($params->quantity)) ? $params->quantity : null;

            $orderdetail = $em->getRepository('BackendBundle:Orderdetail')->findOneBy(array("id" => $id));
            if(count($orderdetail)>0){
                $orderdetail->setQuantity($quantity);

                $em->persist($orderdetail);
                $em->flush();

                $data["status"] = 'success';
                $data["code"] = 200;
                $data["msg"] = 'Detail Updated!!';
                $data["Quantity"] = $quantity;
                $data["ID"] = $id;
            }
        }
        return $helpers->jsonOrder($data);
    }

    public function detailAction(Request $request, $id = null){
        $helpers    = $this->get("app.helpers");
        $em         = $this->getDoctrine()->getManager();
        $solicitud  = $em->getRepository('BackendBundle:Orderdetail')->findBy(array("orderid" => $id));

        $order     = $em->getRepository('BackendBundle:Orders')->findOneBy(array("id" => $id));

        //$orders = new Orders();

        $supplier   = $em->getRepository('BackendBundle:Orderdetail')->findoneBy(array("orderid" => $id));
        $data      = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Order not created",
            "order"=>$order,
            "details"=>$solicitud
        );
        return $helpers->jsonOrder($data);
    }
}





