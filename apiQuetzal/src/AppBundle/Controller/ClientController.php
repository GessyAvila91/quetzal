<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;

use BackendBundle\Entity\Client;
use BackendBundle\Entity\Email;
use BackendBundle\Entity\Phone;
use BackendBundle\Entity\Card;

class ClientController extends Controller{
    //MARK: DEMO functions
    public function indexAction(Request $request){
        $helpers = $this->get("app.helpers");

        $em = $this->getDoctrine()->getManager();
        $solicitud = $em->getRepository('BackendBundle:Client')->findAll();

        return $helpers->jsonClient($solicitud);
    }

    public function gettestAction(Request $request){
        $helpers = $this->get("app.helpers");

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('BackendBundle:Client')->findAll();

        return $helpers->json($users);
    }

    public function posttestAction(Request $request){
        echo("POST TEST");
        die();
        $helpers = $this->get("app.helpers");
        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Client not found"
        );
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('BackendBundle:Client')->findAll();

        return $helpers->json($users);
    }

    //MARK:
    public function infoAction(Request $request,$id = null){
        $helpers = $this->get("app.helpers");

        $em = $this->getDoctrine()->getManager();
        $solicitud = $em->getRepository('BackendBundle:Client')->findOneBy(array('id' => $id));
        if(count($solicitud) >= 1){
            $data = array(
                "status" => "success",
                "code" => 200,
                "msg" => "Client found",
                "data" => $solicitud
            );
            $email = $em->getRepository('BackendBundle:Email')->findBy(array('clientid' => $id));
            $phone = $em->getRepository('BackendBundle:Phone')->findBy(array('clientid' => $id));
            $card  = $em->getRepository('BackendBundle:Card' )->findBy(array('clientid' => $id));
            if(count($email) >= 1){
                $data["email"] = $email;
            }
            if (count($phone) >= 1){
                $data["phone"] = $phone;
            }
            if (count($card) >= 1){
                $data["card"] = $card;
            }
        }else{
            $data = array(
                "status" => "error",
                "code" => 200,
                "msg" => "Client not found"
            );
        }
        return $helpers->jsonClient($data);
    }
    public function phoneAction(Request $request, $id = null){
        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);
        $params = json_decode($json);
        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "phone not added"
        );
        if ($json != null) {
            $clientid      = $id;
            $number        = (isset($params->number))           ? $params->number: null;
            $description   = (isset($params->description))      ? $params->description: null;
            if ($number != null || $description != null || $clientid!= null) {
                $Number = new Phone();
                $Number->setNumber($number);
                $Number->setDescription($description);
                $em = $this->getDoctrine()->getManager();

                $client = $em->getRepository('BackendBundle:Client')->findOneBy(array(
                        "id" => $clientid)
                );

                $Number->setClientid($client);
                $em = $this->getDoctrine()->getManager();
                $em->persist($Number);
                $em->flush();
                $data["status"] = 'success';
                $data["code"] = 200;
                $data["msg"] = 'New phone added !!';
            }else{
                $data["status"] = 'error';
                $data["code"] = 400;
                $data["msg"] = 'Request Null';
            }
        }
        return $helpers->json($data);
    }
    public function phoneeditAction(Request $request){
        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);
        $params = json_decode($json);
        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Phone not Edit"
        );
        if ($json != null) {
            $id               = (isset($params->id))               ? $params->id: null;
            $number           = (isset($params->number))           ? $params->number: null;
            $description      = (isset($params->description))      ? $params->description: null;
            if ($number != null || $description != null || $id != null) {
                $em = $this->getDoctrine()->getManager();
                $Phone = $em->getRepository('BackendBundle:Phone')->findOneBy(array(
                        "id" => $id)
                );
                $Phone->setNumber($number);
                $Phone->setDescription($description);

                $em->persist($Phone);
                $em->flush();

                $data["status"] = 'success';
                $data["code"] = 200;
                $data["msg"] = 'Edited !!';
                $data["inf"] = $Phone;

            }else{
                $data["status"] = 'error';
                $data["code"] = 400;
                $data["msg"] = 'Data Null';
            }
        }
        return $helpers->json($data);
    }
    public function emailAction(Request $request, $id = null){
        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);
        $params = json_decode($json);
        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Email not added"
        );
        if ($json != null) {
            $clientid     = $id;
            $email        = (isset($params->email))            ? $params->email: null;
            $description  = (isset($params->description))      ? $params->description: null;
            if ($email != null || $description != null || $clientid!= null) {
                $em = $this->getDoctrine()->getManager();
                $client = $em->getRepository('BackendBundle:Client')->findOneBy(array(
                        "id" => $clientid)
                );

                $Email = new Email();
                $Email->setEmail($email);
                $Email->setDescription($description);
                $Email->setClientid($client);

                $em = $this->getDoctrine()->getManager();
                $em->persist($Email);
                $em->flush();
                $data["status"] = 'success';
                $data["code"] = 200;
                $data["msg"] = 'New Email added !!';
            }else{
                $data["status"] = 'error';
                $data["code"] = 400;
                $data["msg"] = 'Request Null';
            }
        }
        return $helpers->json($data);
    }
    public function emaileditAction(Request $request){
        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);
        $params = json_decode($json);
        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Email not Edit"
        );
        if ($json != null) {
            $id               = (isset($params->id))               ? $params->id: null;
            $email            = (isset($params->email))            ? $params->email: null;
            $description      = (isset($params->description))      ? $params->description: null;
            if ($email != null || $description != null || $id != null) {
                $em = $this->getDoctrine()->getManager();
                $Email = $em->getRepository('BackendBundle:Email')->findOneBy(array(
                        "id" => $id)
                );
                $Email->setEmail($email);
                $Email->setDescription($description);

                $em->persist($Email);
                $em->flush();

                $data["status"] = 'success';
                $data["code"] = 200;
                $data["msg"] = 'Edited !!';
                $data["inf"] = $Email;

            }else{
                $data["status"] = 'error';
                $data["code"] = 400;
                $data["msg"] = 'Data Null';
            }
        }
        return $helpers->json($data);
    }
    public function cardAction(Request $request, $id = null){

    }
    public function editAction(Request $request){
        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);
        $params = json_decode($json);
        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Client not Edit"
        );
        if ($json != null) {
            $id               = (isset($params->id))               ? $params->id: null;
            $title            = (isset($params->title))            ? $params->title: null;
            $name             = (isset($params->name))             ? $params->name: null;
            $lastnamepaternal = (isset($params->lastnamepaternal)) ? $params->lastnamepaternal: null;
            $lastnamematernal = (isset($params->lastnamematernal)) ? $params->lastnamematernal: null;
            $category         = (isset($params->category))         ? $params->category: null;

            $rfc              = (isset($params->rfc))              ? $params->rfc: null;
            $company          = (isset($params->company))          ? $params->company: null;
            $billadress       = (isset($params->billadress))       ? $params->billadress: null;
            $colony           = (isset($params->colony))           ? $params->colony: null;
            $state            = (isset($params->state))            ? $params->state: null;

            $city             = (isset($params->city))             ? $params->city: null;
            $pc               = (isset($params->pc))               ? $params->pc: null;
            $commentary       = (isset($params->commentary))       ? $params->commentary: null;

            if ($name != null || $lastnamepaternal != null || $lastnamematernal != null) {
                if(true){
                    $em = $this->getDoctrine()->getManager();
                    $Client = $em->getRepository('BackendBundle:Client')->findOneBy(array(
                            "id" => $id)
                    );
                    $Client->setTitle($title);
                    $Client->setName($name);
                    $Client->setLastnamepaternal($lastnamepaternal);
                    $Client->setLastnamematernal($lastnamematernal);
                    $Client->setCategory($category);

                    $Client->setRfc($rfc);
                    $Client->setCompany($company);
                    $Client->setBilladress($billadress);
                    $Client->setColony($colony);
                    $Client->setState($state);

                    $Client->setCity($city);
                    $Client->setPc($pc);
                    $Client->setCommentary($commentary);

                    $em = $this->getDoctrine()->getManager();

                    $em->persist($Client);
                    $em->flush();

                    $data["status"] = 'success';
                    $data["code"] = 200;
                    $data["msg"] = 'New Client created !!';
                    $data["inf"] = $Client;
                }else{
                    $data["status"] = 'error';
                    $data["code"] = 400;
                    $data["msg"] = 'Code Duplicated';
                }
            }else{
                $data["status"] = 'error';
                $data["code"] = 400;
                $data["msg"] = 'Name or Code Null';
            }
        }
        return $helpers->json($data);
    }
    public function newAction(Request $request){
        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);
        $params = json_decode($json);

        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Client not created"
        );

        if ($json != null) {

            $title            = (isset($params->title))            ? $params->title: null;
            $name             = (isset($params->name))             ? $params->name: null;
            $lastnamepaternal = (isset($params->lastnamepaternal)) ? $params->lastnamepaternal: null;
            $lastnamematernal = (isset($params->lastnamematernal)) ? $params->lastnamematernal: null;
            $category         = (isset($params->category))         ? $params->category: null;

            $rfc              = (isset($params->rfc))              ? $params->rfc: null;
            $company          = (isset($params->company))          ? $params->company: null;
            $billadress       = (isset($params->billadress))       ? $params->billadress: null;
            $colony           = (isset($params->colony))           ? $params->colony: null;
            $state            = (isset($params->state))            ? $params->state: null;

            $city             = (isset($params->city))             ? $params->city: null;
            $pc               = (isset($params->pc))               ? $params->pc: null;
            $commentary       = (isset($params->commentary))       ? $params->commentary: null;

            if ($name != null || $lastnamepaternal != null || $lastnamematernal != null) {

                if(true){
                    $Client = new Client();

                    $Client->setTitle($title);
                    $Client->setName($name);
                    $Client->setLastnamepaternal($lastnamepaternal);
                    $Client->setLastnamematernal($lastnamematernal);
                    $Client->setCategory($category);

                    $Client->setRfc($rfc);
                    $Client->setCompany($company);
                    $Client->setBilladress($billadress);
                    $Client->setColony($colony);
                    $Client->setState($state);

                    $Client->setCity($city);
                    $Client->setPc($pc);
                    $Client->setCommentary($commentary);

                    $em = $this->getDoctrine()->getManager();

                    $em->persist($Client);
                    $em->flush();

                    $data["status"] = 'success';
                    $data["code"] = 200;
                    $data["msg"] = 'New Client created !!';
                }else{
                    $data["status"] = 'error';
                    $data["code"] = 400;
                    $data["msg"] = 'Code Duplicated';
                }

            }else{
                $data["status"] = 'error';
                $data["code"] = 400;
                $data["msg"] = 'Name or Code Null';
            }
        }
        return $helpers->json($data);
    }

}