<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;

use BackendBundle\Entity\Category;

class CategoryController extends Controller{
    //MARK: DEMO functions
    public function indexAction(Request $request){
        $helpers = $this->get("app.helpers");
        $em = $this->getDoctrine()->getManager();
        $solicitud = $em->getRepository('BackendBundle:Category')->findAll();
        return $helpers->jsonRawmatter($solicitud);
    }

    public function gettestAction(Request $request){
        $helpers = $this->get("app.helpers");
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('BackendBundle:Category')->findAll();
        return $helpers->json($users);
    }

    public function posttestAction(Request $request){
        $helpers = $this->get("app.helpers");
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('BackendBundle:Category')->findAll();
        return $helpers->json($users);
    }

    //MARK:
    public function infoAction(Request $request, $code = null){
        $helpers = $this->get("app.helpers");
        $em = $this->getDoctrine()->getManager();
        $solicitud = $em->getRepository('BackendBundle:Category')->findBy(array('rawmattercode' => $code));
        $rm= $em->getRepository('BackendBundle:Rawmatter')->findOneBy(array('code' => $code));
        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Error not found",
            "rawmatter" => $rm
        );
        if(count($solicitud )>= 1){
            $data = array(
                "status" => "success",
                "code" => 200,
                "msg" => "Rawmatter Category founds",
                "rawmatter" => $rm,
                "data" => $solicitud
            );
        }
        return $helpers->jsonRawmatter($data);
    }

    public function newAction(Request $request){
        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);
        $params = json_decode($json);
        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Category not created"
        );
        if ($json != null) {
            $helpers       = $this->get("app.helpers");
            $code          = (isset($params->code))           ? $params->code: null;
            $rawmatterCode = (isset($params->rawmatterCode )) ? $params->rawmatterCode : null;
            $name          = (isset($params->name))           ? $params->name: null;
            $description   = (isset($params->description))    ? $params->description: null;
            if ($code != null || $rawmatterCode != null || $name != null) {
                if(true){
                    $em = $this->getDoctrine()->getManager();
                    $solicitud = $em->getRepository('BackendBundle:Rawmatter')->findOneBy(array(
                            "code" => $rawmatterCode)
                    );
                    $category = new Category();
                    $category->setCode($code);
                    $category->setRawmattercode($solicitud);
                    $category->setName($name);
                    $category->setDecription($description);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($category);
                    $em->flush();

                    $data["status"] = 'success';
                    $data["code"] = 200;
                    $data["msg"] = 'New Category created !!';
                }else{
                    $data["status"] = 'error';
                    $data["code"] = 400;
                    $data["msg"] = 'Code Duplicated';
                }
            }else{
                $data["status"] = 'error';
                $data["code"] = 400;
                $data["msg"] = 'Name or Code Null';
            }
        }
        return $helpers->json($data);
    }
}