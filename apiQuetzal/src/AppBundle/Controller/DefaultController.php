<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller{

    public function indexAction(Request $request){
        //echo("DefaultController");
        $helpers = $this->get("app.helpers");

        $em = $this->getDoctrine()->getManager();


        $solicitud= $em->getRepository('BackendBundle:Rawmatter')->findOneBy(array("code" => "AGM000"));
        return $helpers->json($solicitud);
    }

    public function gettestAction(Request $request){
        echo("GET TEST");
        die();
        $helpers = $this->get("app.helpers");

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('BackendBundle:Usuario')->findAll();

        return $helpers->json($users);
    }

    public function posttestAction(Request $request){
        echo("POST TEST");
        die();
        $helpers = $this->get("app.helpers");

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('BackendBundle:Usuario')->findAll();

        return $helpers->json($users);
    }

}
