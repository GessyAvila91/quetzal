<?php
/**
 * Created by PhpStorm.
 * User: GessyAvila
 * Date: 28-Jul-17
 * Time: 3:37 AM
 */

namespace AppBundle\Controller;

use BackendBundle\Entity\Categorymatter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerAggregate;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;

use BackendBundle\Entity\Seccion;

class SeccionController extends Controller {
    //MARK: DEMO functions
    public function indexAction(Request $request){
        $helpers = $this->get("app.helpers");

        $em = $this->getDoctrine()->getManager();
        $solicitud = $em->getRepository('BackendBundle:Seccion')->findAll();

        return $helpers->jsonSeccion($solicitud);
    }

    public function gettestAction(Request $request){
        $helpers = $this->get("app.helpers");

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('BackendBundle:Seccion')->findAll();

        return $helpers->json($users);
    }

    public function posttestAction(Request $request){
        $helpers = $this->get("app.helpers");
        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Seccion not found"
        );
        $em = $this->getDoctrine()->getManager();
        $seccion = $em->getRepository('BackendBundle:Seccion')->findAll();

        return $helpers->json($seccion);
    }
    public function editmatterAction(Request $request){
        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);
        $params = json_decode($json);
        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Category not Added"
        );
        if($json != null){
            $idr = (isset($params->idr)) ? $params->idr: null;
            $quantity= (isset($params->quantity)) ? $params->quantity: null;
            if($quantity >=0){
                $em = $this->getDoctrine()->getManager();
                $Categorymatter = $em->getRepository('BackendBundle:Categorymatter')->findOneBy(array(
                    "id" => $idr
                ));
                $Categorymatter->setQuantity($quantity);
                if($quantity > 0){
                    $em->persist($Categorymatter);
                    $em->flush();
                    $data = array(
                        "status" => "success",
                        "code" => 200,
                        "msg" => "Matter Edit"
                    );
                }
                if($quantity == 0){
                    $em->remove($Categorymatter);
                    $em->flush();
                    $data = array(
                        "status" => "success",
                        "code" => 200,
                        "msg" => "Matter Remove"
                    );
                }
            }else{
                $data = array(
                    "status" => "error",
                    "code" => 400,
                    "msg" => "Quantity < 0"
                );
            }
        }
        return $helpers->json($data);
    }

    public function addAction(Request $request){
        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);
        $params = json_decode($json);
        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Category not Added"
        );
        if($json != null){
            $id = (isset($params->id)) ? $params->id: null;
            $code = (isset($params->code)) ? $params->code: null;
            $quantity = (isset($params->quantity)) ? $params->quantity: null;
            $em = $this->getDoctrine()->getManager();
            $seccion = $em->getRepository('BackendBundle:Seccion')->findOneBy(array(
                "id" => $id
            ));
            $category= $em->getRepository('BackendBundle:Category')->findOneBy(array(
                "code" => $code
            ));
            if(count($category) > 0){
                $CM= $em->getRepository('BackendBundle:Categorymatter')->findOneBy(array(
                    "seccionid" => $id,
                    "categorycode" => $code
                ));
                if(count($CM)>0){
                    $quantity = $quantity + $CM->getQuantity();
                    $CM->setQuantity($quantity);
                    $em->persist($CM);
                    $em->flush();
                    $data = array(
                        "status" => "success",
                        "code" => 200,
                        "msg" => "Category Added to Seccion"
                    );
                }else{
                    $Categorymatter = new Categorymatter();
                    $Categorymatter->setCategorycode($category);
                    $Categorymatter->setSeccionid($seccion);
                    $Categorymatter->setQuantity($quantity);
                    $em->persist($Categorymatter);
                    $em->flush();
                    $data = array(
                        "status" => "success",
                        "code" => 200,
                        "msg" => "Category Added to Seccion"
                    );
                }
            }else{
                $data = array(
                    "status" => "error",
                    "code" => 400,
                    "msg" => "Category Not Added"
                );
            }
        }
        return $helpers->json($data);
    }

    public function contentAction(Request $request, $id = null) {
        $helpers = $this->get("app.helpers");
        $em = $this->getDoctrine()->getManager();

        $seccioninf = $em->getRepository("BackendBundle:Seccion")->findOneBy(array(
            "id" => $id
        ));
        $producto = $em->getRepository("BackendBundle:Categorymatter")->findBy(array(
            "seccionid" => $seccioninf
        ), array('id'=>'asc'));

        if(count($producto)>=1){
            $data = array(
                "status" => "success",
                "code"	 => 200,
                "seccion" => $seccioninf,
                "data"	 => $producto
            );
        }else{
            $data = array(
                "status" => "error",
                "code"	 => 400,
                "seccion" => $seccioninf,
                "msg"	 => "Dont exists productos in this seccion!!"
            );
        }
        return $helpers->jsonSeccion($data);
    }
    public function newAction(Request $request){
        $helpers = $this->get("app.helpers");

        $json = $request->get("json", null);
        $params = json_decode($json);

        $data = array(
            "status" => "error",
            "code" => 400,
            "msg" => "Seccion not created"
        );

        if ($json != null) {

            $name = (isset($params->name)) ? $params->name: null;
            $description= (isset($params->description)) ? $params->description: null;

            if ($name != null) {
                $seccion = new Seccion();

                $seccion->setName($name);
                $seccion->setDescription($description);

                $em = $this->getDoctrine()->getManager();
                $em->persist($seccion);
                $em->flush();

                $data["status"] = 'success';
                $data["code"] = 200;
                $data["msg"] = 'New seccion created !!';
            }else{
                $data["status"] = 'error';
                $data["code"] = 400;
                $data["msg"] = 'Name Null';
            }
        }
        return $helpers->json($data);
    }

    public function infoAction(Request $request, $id = null) {
        $helpers = $this->get("app.helpers");
        $em = $this->getDoctrine()->getManager();

        $seccioninf = $em->getRepository("BackendBundle:Seccion")->findOneBy(array(
            "id" => $id
        ));
        return $helpers->jsonSeccion($seccioninf);
    }

    public function editAction(Request $request) {
        $helpers = $this->get("app.helpers");
        $json = $request->get("json", null);

        if ($json != null) {
            $params = json_decode($json);

            $id          = (isset($params->id)) ?          $params->id: null;
            $name        = (isset($params->name)) ?      $params->name: null;
            $description = (isset($params->description)) ? $params->description: null;

            if ($name != null || $description != null || $id != null) {
                $em = $this->getDoctrine()->getManager();

                $seccion = $em->getRepository("BackendBundle:Seccion")->findOneBy(array(
                        "id" => $id)
                );
                $seccion->setName($name);
                $seccion->setDescription($description);

                $em = $this->getDoctrine()->getManager();

                $em->persist($seccion);
                $em->flush();

                $data = array(
                    "status" => "success",
                    "code" => 200,
                    "msg" => "Seccion actualizado success!!"
                );

            } else {
                $data = array(
                    "status" => "error",
                    "code" => 400,
                    "msg" => "Seccion actualizado  error"
                );
            }
        } else {
            $data = array(
                "status" => "error",
                "code" => 400,
                "msg" => "Seccion not actualizado , params failed"
            );
        }
        return $helpers->json($data);
    }

}