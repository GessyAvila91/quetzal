<?php

namespace BackendBundle\Entity;

/**
 * Recipe
 */
class Recipe
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var \BackendBundle\Entity\Product
     */
    private $productcode;

    /**
     * @var \BackendBundle\Entity\Rawmatter
     */
    private $rawmattercode;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return Recipe
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set productcode
     *
     * @param \BackendBundle\Entity\Product $productcode
     *
     * @return Recipe
     */
    public function setProductcode(\BackendBundle\Entity\Product $productcode = null)
    {
        $this->productcode = $productcode;

        return $this;
    }

    /**
     * Get productcode
     *
     * @return \BackendBundle\Entity\Product
     */
    public function getProductcode()
    {
        return $this->productcode;
    }

    /**
     * Set rawmattercode
     *
     * @param \BackendBundle\Entity\Rawmatter $rawmattercode
     *
     * @return Recipe
     */
    public function setRawmattercode(\BackendBundle\Entity\Rawmatter $rawmattercode = null)
    {
        $this->rawmattercode = $rawmattercode;

        return $this;
    }

    /**
     * Get rawmattercode
     *
     * @return \BackendBundle\Entity\Rawmatter
     */
    public function getRawmattercode()
    {
        return $this->rawmattercode;
    }
}
