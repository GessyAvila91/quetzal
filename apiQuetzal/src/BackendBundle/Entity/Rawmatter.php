<?php

namespace BackendBundle\Entity;

/**
 * Rawmatter
 */
class Rawmatter
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $price;

    /**
     * @var integer
     */
    private $minreorder;

    /**
     * @var string
     */
    private $maxstock;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $ordersid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $supplierid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ordersid = new \Doctrine\Common\Collections\ArrayCollection();
        $this->supplierid = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Set code
     *
     * @param string $code
     *
     * @return Rawmatter
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }
    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Rawmatter
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Rawmatter
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Rawmatter
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set minreorder
     *
     * @param integer $minreorder
     *
     * @return Rawmatter
     */
    public function setMinreorder($minreorder)
    {
        $this->minreorder = $minreorder;

        return $this;
    }

    /**
     * Get minreorder
     *
     * @return integer
     */
    public function getMinreorder()
    {
        return $this->minreorder;
    }

    /**
     * Set maxstock
     *
     * @param string $maxstock
     *
     * @return Rawmatter
     */
    public function setMaxstock($maxstock)
    {
        $this->maxstock = $maxstock;

        return $this;
    }

    /**
     * Get maxstock
     *
     * @return string
     */
    public function getMaxstock()
    {
        return $this->maxstock;
    }

    /**
     * Add ordersid
     *
     * @param \BackendBundle\Entity\Orders $ordersid
     *
     * @return Rawmatter
     */
    public function addOrdersid(\BackendBundle\Entity\Orders $ordersid)
    {
        $this->ordersid[] = $ordersid;

        return $this;
    }

    /**
     * Remove ordersid
     *
     * @param \BackendBundle\Entity\Orders $ordersid
     */
    public function removeOrdersid(\BackendBundle\Entity\Orders $ordersid)
    {
        $this->ordersid->removeElement($ordersid);
    }

    /**
     * Get ordersid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrdersid()
    {
        return $this->ordersid;
    }

    /**
     * Add supplierid
     *
     * @param \BackendBundle\Entity\Supplier $supplierid
     *
     * @return Rawmatter
     */
    public function addSupplierid(\BackendBundle\Entity\Supplier $supplierid)
    {
        $this->supplierid[] = $supplierid;

        return $this;
    }

    /**
     * Remove supplierid
     *
     * @param \BackendBundle\Entity\Supplier $supplierid
     */
    public function removeSupplierid(\BackendBundle\Entity\Supplier $supplierid)
    {
        $this->supplierid->removeElement($supplierid);
    }

    /**
     * Get supplierid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupplierid()
    {
        return $this->supplierid;
    }
}
