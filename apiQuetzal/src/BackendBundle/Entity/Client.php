<?php

namespace BackendBundle\Entity;

/**
 * Client
 */
class Client
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $lastnamepaternal;

    /**
     * @var string
     */
    private $lastnamematernal;

    /**
     * @var string
     */
    private $category;

    /**
     * @var string
     */
    private $rfc;

    /**
     * @var string
     */
    private $company;

    /**
     * @var string
     */
    private $billadress;

    /**
     * @var string
     */
    private $colony;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $pc;

    /**
     * @var string
     */
    private $commentary;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Client
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastnamepaternal
     *
     * @param string $lastnamepaternal
     *
     * @return Client
     */
    public function setLastnamepaternal($lastnamepaternal)
    {
        $this->lastnamepaternal = $lastnamepaternal;

        return $this;
    }

    /**
     * Get lastnamepaternal
     *
     * @return string
     */
    public function getLastnamepaternal()
    {
        return $this->lastnamepaternal;
    }

    /**
     * Set lastnamematernal
     *
     * @param string $lastnamematernal
     *
     * @return Client
     */
    public function setLastnamematernal($lastnamematernal)
    {
        $this->lastnamematernal = $lastnamematernal;

        return $this;
    }

    /**
     * Get lastnamematernal
     *
     * @return string
     */
    public function getLastnamematernal()
    {
        return $this->lastnamematernal;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return Client
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set rfc
     *
     * @param string $rfc
     *
     * @return Client
     */
    public function setRfc($rfc)
    {
        $this->rfc = $rfc;

        return $this;
    }

    /**
     * Get rfc
     *
     * @return string
     */
    public function getRfc()
    {
        return $this->rfc;
    }

    /**
     * Set company
     *
     * @param string $company
     *
     * @return Client
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set billadress
     *
     * @param string $billadress
     *
     * @return Client
     */
    public function setBilladress($billadress)
    {
        $this->billadress = $billadress;

        return $this;
    }

    /**
     * Get billadress
     *
     * @return string
     */
    public function getBilladress()
    {
        return $this->billadress;
    }

    /**
     * Set colony
     *
     * @param string $colony
     *
     * @return Client
     */
    public function setColony($colony)
    {
        $this->colony = $colony;

        return $this;
    }

    /**
     * Get colony
     *
     * @return string
     */
    public function getColony()
    {
        return $this->colony;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Client
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Client
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set pc
     *
     * @param string $pc
     *
     * @return Client
     */
    public function setPc($pc)
    {
        $this->pc = $pc;

        return $this;
    }

    /**
     * Get pc
     *
     * @return string
     */
    public function getPc()
    {
        return $this->pc;
    }

    /**
     * Set commentary
     *
     * @param string $commentary
     *
     * @return Client
     */
    public function setCommentary($commentary)
    {
        $this->commentary = $commentary;

        return $this;
    }

    /**
     * Get commentary
     *
     * @return string
     */
    public function getCommentary()
    {
        return $this->commentary;
    }
}
