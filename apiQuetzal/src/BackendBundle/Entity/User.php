<?php

namespace BackendBundle\Entity;

/**
 * User
 */
class User
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $nickname;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $lastnamepaternal;

    /**
     * @var string
     */
    private $lastnamematernal;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $rolid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $seccionid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rolid = new \Doctrine\Common\Collections\ArrayCollection();
        $this->seccionid = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set nickname
     *
     * @param string $nickname
     *
     * @return User
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * Get nickname
     *
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastnamepaternal
     *
     * @param string $lastnamepaternal
     *
     * @return User
     */
    public function setLastnamepaternal($lastnamepaternal)
    {
        $this->lastnamepaternal = $lastnamepaternal;

        return $this;
    }

    /**
     * Get lastnamepaternal
     *
     * @return string
     */
    public function getLastnamepaternal()
    {
        return $this->lastnamepaternal;
    }

    /**
     * Set lastnamematernal
     *
     * @param string $lastnamematernal
     *
     * @return User
     */
    public function setLastnamematernal($lastnamematernal)
    {
        $this->lastnamematernal = $lastnamematernal;

        return $this;
    }

    /**
     * Get lastnamematernal
     *
     * @return string
     */
    public function getLastnamematernal()
    {
        return $this->lastnamematernal;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return User
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add rolid
     *
     * @param \BackendBundle\Entity\Rol $rolid
     *
     * @return User
     */
    public function addRolid(\BackendBundle\Entity\Rol $rolid)
    {
        $this->rolid[] = $rolid;

        return $this;
    }

    /**
     * Remove rolid
     *
     * @param \BackendBundle\Entity\Rol $rolid
     */
    public function removeRolid(\BackendBundle\Entity\Rol $rolid)
    {
        $this->rolid->removeElement($rolid);
    }

    /**
     * Get rolid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRolid()
    {
        return $this->rolid;
    }

    /**
     * Add seccionid
     *
     * @param \BackendBundle\Entity\Seccion $seccionid
     *
     * @return User
     */
    public function addSeccionid(\BackendBundle\Entity\Seccion $seccionid)
    {
        $this->seccionid[] = $seccionid;

        return $this;
    }

    /**
     * Remove seccionid
     *
     * @param \BackendBundle\Entity\Seccion $seccionid
     */
    public function removeSeccionid(\BackendBundle\Entity\Seccion $seccionid)
    {
        $this->seccionid->removeElement($seccionid);
    }

    /**
     * Get seccionid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSeccionid()
    {
        return $this->seccionid;
    }
}
