<?php

namespace BackendBundle\Entity;

/**
 * Rol
 */
class Rol
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $descriotion;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userid = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Rol
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set descriotion
     *
     * @param string $descriotion
     *
     * @return Rol
     */
    public function setDescriotion($descriotion)
    {
        $this->descriotion = $descriotion;

        return $this;
    }

    /**
     * Get descriotion
     *
     * @return string
     */
    public function getDescriotion()
    {
        return $this->descriotion;
    }

    /**
     * Add userid
     *
     * @param \BackendBundle\Entity\User $userid
     *
     * @return Rol
     */
    public function addUserid(\BackendBundle\Entity\User $userid)
    {
        $this->userid[] = $userid;

        return $this;
    }

    /**
     * Remove userid
     *
     * @param \BackendBundle\Entity\User $userid
     */
    public function removeUserid(\BackendBundle\Entity\User $userid)
    {
        $this->userid->removeElement($userid);
    }

    /**
     * Get userid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserid()
    {
        return $this->userid;
    }
}
