<?php

namespace BackendBundle\Entity;

/**
 * Supplier
 */
class Supplier
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $tradename;

    /**
     * @var string
     */
    private $businessname;

    /**
     * @var string
     */
    private $contactperson;

    /**
     * @var string
     */
    private $addres;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $webpage;

    /**
     * @var string
     */
    private $rfc;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $rawmattercode;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rawmattercode = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tradename
     *
     * @param string $tradename
     *
     * @return Supplier
     */
    public function setTradename($tradename)
    {
        $this->tradename = $tradename;

        return $this;
    }

    /**
     * Get tradename
     *
     * @return string
     */
    public function getTradename()
    {
        return $this->tradename;
    }

    /**
     * Set businessname
     *
     * @param string $businessname
     *
     * @return Supplier
     */
    public function setBusinessname($businessname)
    {
        $this->businessname = $businessname;

        return $this;
    }

    /**
     * Get businessname
     *
     * @return string
     */
    public function getBusinessname()
    {
        return $this->businessname;
    }

    /**
     * Set contactperson
     *
     * @param string $contactperson
     *
     * @return Supplier
     */
    public function setContactperson($contactperson)
    {
        $this->contactperson = $contactperson;

        return $this;
    }

    /**
     * Get contactperson
     *
     * @return string
     */
    public function getContactperson()
    {
        return $this->contactperson;
    }

    /**
     * Set addres
     *
     * @param string $addres
     *
     * @return Supplier
     */
    public function setAddres($addres)
    {
        $this->addres = $addres;

        return $this;
    }

    /**
     * Get addres
     *
     * @return string
     */
    public function getAddres()
    {
        return $this->addres;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Supplier
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Supplier
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set webpage
     *
     * @param string $webpage
     *
     * @return Supplier
     */
    public function setWebpage($webpage)
    {
        $this->webpage = $webpage;

        return $this;
    }

    /**
     * Get webpage
     *
     * @return string
     */
    public function getWebpage()
    {
        return $this->webpage;
    }

    /**
     * Set rfc
     *
     * @param string $rfc
     *
     * @return Supplier
     */
    public function setRfc($rfc)
    {
        $this->rfc = $rfc;

        return $this;
    }

    /**
     * Get rfc
     *
     * @return string
     */
    public function getRfc()
    {
        return $this->rfc;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Supplier
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add rawmattercode
     *
     * @param \BackendBundle\Entity\Rawmatter $rawmattercode
     *
     * @return Supplier
     */
    public function addRawmattercode(\BackendBundle\Entity\Rawmatter $rawmattercode)
    {
        $this->rawmattercode[] = $rawmattercode;

        return $this;
    }

    /**
     * Remove rawmattercode
     *
     * @param \BackendBundle\Entity\Rawmatter $rawmattercode
     */
    public function removeRawmattercode(\BackendBundle\Entity\Rawmatter $rawmattercode)
    {
        $this->rawmattercode->removeElement($rawmattercode);
    }

    /**
     * Get rawmattercode
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRawmattercode()
    {
        return $this->rawmattercode;
    }
}
