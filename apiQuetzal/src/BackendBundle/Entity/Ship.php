<?php

namespace BackendBundle\Entity;

/**
 * Ship
 */
class Ship
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $deliberydate;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $recives;

    /**
     * @var \BackendBundle\Entity\Sell
     */
    private $sellid;

    /**
     * @var \BackendBundle\Entity\User
     */
    private $deliverid;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deliberydate
     *
     * @param \DateTime $deliberydate
     *
     * @return Ship
     */
    public function setDeliberydate($deliberydate)
    {
        $this->deliberydate = $deliberydate;

        return $this;
    }

    /**
     * Get deliberydate
     *
     * @return \DateTime
     */
    public function getDeliberydate()
    {
        return $this->deliberydate;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Ship
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set recives
     *
     * @param string $recives
     *
     * @return Ship
     */
    public function setRecives($recives)
    {
        $this->recives = $recives;

        return $this;
    }

    /**
     * Get recives
     *
     * @return string
     */
    public function getRecives()
    {
        return $this->recives;
    }

    /**
     * Set sellid
     *
     * @param \BackendBundle\Entity\Sell $sellid
     *
     * @return Ship
     */
    public function setSellid(\BackendBundle\Entity\Sell $sellid = null)
    {
        $this->sellid = $sellid;

        return $this;
    }

    /**
     * Get sellid
     *
     * @return \BackendBundle\Entity\Sell
     */
    public function getSellid()
    {
        return $this->sellid;
    }

    /**
     * Set deliverid
     *
     * @param \BackendBundle\Entity\User $deliverid
     *
     * @return Ship
     */
    public function setDeliverid(\BackendBundle\Entity\User $deliverid = null)
    {
        $this->deliverid = $deliverid;

        return $this;
    }

    /**
     * Get deliverid
     *
     * @return \BackendBundle\Entity\User
     */
    public function getDeliverid()
    {
        return $this->deliverid;
    }
}
