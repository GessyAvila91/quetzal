<?php

namespace BackendBundle\Entity;

/**
 * Sell
 */
class Sell
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \BackendBundle\Entity\Client
     */
    private $clientid;

    /**
     * @var \BackendBundle\Entity\User
     */
    private $userid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $productcode;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productcode = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Sell
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Sell
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set clientid
     *
     * @param \BackendBundle\Entity\Client $clientid
     *
     * @return Sell
     */
    public function setClientid(\BackendBundle\Entity\Client $clientid = null)
    {
        $this->clientid = $clientid;

        return $this;
    }

    /**
     * Get clientid
     *
     * @return \BackendBundle\Entity\Client
     */
    public function getClientid()
    {
        return $this->clientid;
    }

    /**
     * Set userid
     *
     * @param \BackendBundle\Entity\User $userid
     *
     * @return Sell
     */
    public function setUserid(\BackendBundle\Entity\User $userid = null)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return \BackendBundle\Entity\User
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Add productcode
     *
     * @param \BackendBundle\Entity\Product $productcode
     *
     * @return Sell
     */
    public function addProductcode(\BackendBundle\Entity\Product $productcode)
    {
        $this->productcode[] = $productcode;

        return $this;
    }

    /**
     * Remove productcode
     *
     * @param \BackendBundle\Entity\Product $productcode
     */
    public function removeProductcode(\BackendBundle\Entity\Product $productcode)
    {
        $this->productcode->removeElement($productcode);
    }

    /**
     * Get productcode
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductcode()
    {
        return $this->productcode;
    }
}
