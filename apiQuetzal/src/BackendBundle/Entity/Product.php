<?php

namespace BackendBundle\Entity;

/**
 * Product
 */
class Product
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $price;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $sellid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sellid = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Add sellid
     *
     * @param \BackendBundle\Entity\Sell $sellid
     *
     * @return Product
     */
    public function addSellid(\BackendBundle\Entity\Sell $sellid)
    {
        $this->sellid[] = $sellid;

        return $this;
    }

    /**
     * Remove sellid
     *
     * @param \BackendBundle\Entity\Sell $sellid
     */
    public function removeSellid(\BackendBundle\Entity\Sell $sellid)
    {
        $this->sellid->removeElement($sellid);
    }

    /**
     * Get sellid
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSellid()
    {
        return $this->sellid;
    }
}
