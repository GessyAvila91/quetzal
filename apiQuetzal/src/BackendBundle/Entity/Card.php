<?php

namespace BackendBundle\Entity;

/**
 * Card
 */
class Card
{
    /**
     * @var integer
     */
    private $cardcol;

    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $cvv;

    /**
     * @var string
     */
    private $expirationdate;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var \BackendBundle\Entity\Client
     */
    private $clientid;


    /**
     * Get cardcol
     *
     * @return integer
     */
    public function getCardcol()
    {
        return $this->cardcol;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Card
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set cvv
     *
     * @param string $cvv
     *
     * @return Card
     */
    public function setCvv($cvv)
    {
        $this->cvv = $cvv;

        return $this;
    }

    /**
     * Get cvv
     *
     * @return string
     */
    public function getCvv()
    {
        return $this->cvv;
    }

    /**
     * Set expirationdate
     *
     * @param string $expirationdate
     *
     * @return Card
     */
    public function setExpirationdate($expirationdate)
    {
        $this->expirationdate = $expirationdate;

        return $this;
    }

    /**
     * Get expirationdate
     *
     * @return string
     */
    public function getExpirationdate()
    {
        return $this->expirationdate;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Card
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set clientid
     *
     * @param \BackendBundle\Entity\Client $clientid
     *
     * @return Card
     */
    public function setClientid(\BackendBundle\Entity\Client $clientid = null)
    {
        $this->clientid = $clientid;

        return $this;
    }

    /**
     * Get clientid
     *
     * @return \BackendBundle\Entity\Client
     */
    public function getClientid()
    {
        return $this->clientid;
    }
    /**
     * @var integer
     */
    private $id;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
