<?php

namespace BackendBundle\Entity;

/**
 * Trash
 */
class Trash
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var string
     */
    private $commentary;

    /**
     * @var \BackendBundle\Entity\Category
     */
    private $categorycode;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Trash
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return Trash
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set commentary
     *
     * @param string $commentary
     *
     * @return Trash
     */
    public function setCommentary($commentary)
    {
        $this->commentary = $commentary;

        return $this;
    }

    /**
     * Get commentary
     *
     * @return string
     */
    public function getCommentary()
    {
        return $this->commentary;
    }

    /**
     * Set categorycode
     *
     * @param \BackendBundle\Entity\Category $categorycode
     *
     * @return Trash
     */
    public function setCategorycode(\BackendBundle\Entity\Category $categorycode = null)
    {
        $this->categorycode = $categorycode;

        return $this;
    }

    /**
     * Get categorycode
     *
     * @return \BackendBundle\Entity\Category
     */
    public function getCategorycode()
    {
        return $this->categorycode;
    }
}
