<?php

namespace BackendBundle\Entity;

/**
 * Categorymatter
 */
class Categorymatter
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var \BackendBundle\Entity\Category
     */
    private $categorycode;

    /**
     * @var \BackendBundle\Entity\Seccion
     */
    private $seccionid;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return Categorymatter
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set categorycode
     *
     * @param \BackendBundle\Entity\Category $categorycode
     *
     * @return Categorymatter
     */
    public function setCategorycode(\BackendBundle\Entity\Category $categorycode = null)
    {
        $this->categorycode = $categorycode;

        return $this;
    }

    /**
     * Get categorycode
     *
     * @return \BackendBundle\Entity\Category
     */
    public function getCategorycode()
    {
        return $this->categorycode;
    }

    /**
     * Set seccionid
     *
     * @param \BackendBundle\Entity\Seccion $seccionid
     *
     * @return Categorymatter
     */
    public function setSeccionid(\BackendBundle\Entity\Seccion $seccionid = null)
    {
        $this->seccionid = $seccionid;

        return $this;
    }

    /**
     * Get seccionid
     *
     * @return \BackendBundle\Entity\Seccion
     */
    public function getSeccionid()
    {
        return $this->seccionid;
    }
}
