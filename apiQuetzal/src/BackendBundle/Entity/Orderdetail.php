<?php

namespace BackendBundle\Entity;

/**
 * Orderdetail
 */
class Orderdetail
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var string
     */
    private $unitprice;

    /**
     * @var \BackendBundle\Entity\Orders
     */
    private $ordersid;

    /**
     * @var \BackendBundle\Entity\Rawmatter
     */
    private $rawmattercode;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return Orderdetail
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set unitprice
     *
     * @param string $unitprice
     *
     * @return Orderdetail
     */
    public function setUnitprice($unitprice)
    {
        $this->unitprice = $unitprice;

        return $this;
    }

    /**
     * Get unitprice
     *
     * @return string
     */
    public function getUnitprice()
    {
        return $this->unitprice;
    }

    /**
     * Set ordersid
     *
     * @param \BackendBundle\Entity\Orders $ordersid
     *
     * @return Orderdetail
     */
    public function setOrdersid(\BackendBundle\Entity\Orders $ordersid = null)
    {
        $this->ordersid = $ordersid;

        return $this;
    }

    /**
     * Get ordersid
     *
     * @return \BackendBundle\Entity\Orders
     */
    public function getOrdersid()
    {
        return $this->ordersid;
    }

    /**
     * Set rawmattercode
     *
     * @param \BackendBundle\Entity\Rawmatter $rawmattercode
     *
     * @return Orderdetail
     */
    public function setRawmattercode(\BackendBundle\Entity\Rawmatter $rawmattercode = null)
    {
        $this->rawmattercode = $rawmattercode;

        return $this;
    }

    /**
     * Get rawmattercode
     *
     * @return \BackendBundle\Entity\Rawmatter
     */
    public function getRawmattercode()
    {
        return $this->rawmattercode;
    }
    /**
     * @var \BackendBundle\Entity\Orders
     */
    private $orderid;


    /**
     * Set orderid
     *
     * @param \BackendBundle\Entity\Orders $orderid
     *
     * @return Orderdetail
     */
    public function setOrderid(\BackendBundle\Entity\Orders $orderid = null)
    {
        $this->orderid = $orderid;

        return $this;
    }

    /**
     * Get orderid
     *
     * @return \BackendBundle\Entity\Orders
     */
    public function getOrderid()
    {
        return $this->orderid;
    }
}
