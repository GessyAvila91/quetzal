<?php

namespace BackendBundle\Entity;

/**
 * Orders
 */
class Orders
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $registerdate;

    /**
     * @var \DateTime
     */
    private $deliverdate;

    /**
     * @var \DateTime
     */
    private $receptiondate;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \BackendBundle\Entity\Supplier
     */
    private $supplierid;

    /**
     * @var \BackendBundle\Entity\User
     */
    private $userid;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set registerdate
     *
     * @param \DateTime $registerdate
     *
     * @return Orders
     */
    public function setRegisterdate($registerdate)
    {
        $this->registerdate = $registerdate;

        return $this;
    }

    /**
     * Get registerdate
     *
     * @return \DateTime
     */
    public function getRegisterdate()
    {
        return $this->registerdate;
    }

    /**
     * Set deliverdate
     *
     * @param \DateTime $deliverdate
     *
     * @return Orders
     */
    public function setDeliverdate($deliverdate)
    {
        $this->deliverdate = $deliverdate;

        return $this;
    }

    /**
     * Get deliverdate
     *
     * @return \DateTime
     */
    public function getDeliverdate()
    {
        return $this->deliverdate;
    }

    /**
     * Set receptiondate
     *
     * @param \DateTime $receptiondate
     *
     * @return Orders
     */
    public function setReceptiondate($receptiondate)
    {
        $this->receptiondate = $receptiondate;

        return $this;
    }

    /**
     * Get receptiondate
     *
     * @return \DateTime
     */
    public function getReceptiondate()
    {
        return $this->receptiondate;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Orders
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set supplierid
     *
     * @param \BackendBundle\Entity\Supplier $supplierid
     *
     * @return Orders
     */
    public function setSupplierid(\BackendBundle\Entity\Supplier $supplierid = null)
    {
        $this->supplierid = $supplierid;

        return $this;
    }

    /**
     * Get supplierid
     *
     * @return \BackendBundle\Entity\Supplier
     */
    public function getSupplierid()
    {
        return $this->supplierid;
    }

    /**
     * Set userid
     *
     * @param \BackendBundle\Entity\User $userid
     *
     * @return Orders
     */
    public function setUserid(\BackendBundle\Entity\User $userid = null)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return \BackendBundle\Entity\User
     */
    public function getUserid()
    {
        return $this->userid;
    }
    /**
     * @var integer
     */
    private $status = '0';


    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Orders
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
}
