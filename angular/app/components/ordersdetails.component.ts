/**
 * Created by Gessy on 20/04/2017.
 */
/**
 * Created by Gessy on 20/04/2017.
 */
/**
 * Created by getsemaniavila on 12/19/16.
 */
import {Component,OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";
import {OrdersDetailsService} from '../services/ordersdetails.service';
import {Department} from "../model/department";
import {Asignorder} from "../model/asignorder";


@Component({
    selector: 'department',
    templateUrl: 'app/view/ordersdetails.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [OrdersDetailsService]
})

export class OrdersDetailsComponent{
    public titulo="Detalles de Ordenes";
    public department:Department;
    public status;
    public errorMessage;
    public detailsList;
    public categoryList;
    public seccionList;
    public asign ;
    public asignList = [];
    public asignListAux = [];
    public asignorder : Asignorder;

    public orderdata;

    public quantity = 0;
    public RAW;
    public id;

    public seccionid;
    public rawmattercode;
    public rawmattername;
    public rawmatterquantity = 0;

    public categorycode;
    public asignquantity;

    public ORDER;

    constructor(
        private _orderdetailsService: OrdersDetailsService,
        private _route: ActivatedRoute,
        private _router:Router
    ){}

    ngOnInit(){
        this.asignorder = new Asignorder(null,null);
        this.department = new Department(1,"","");
        this.seccionid = 0;
        this.RAW = {
            "id":"",
            "quantity":"",
            "code":"",
            "name":""
        };
        this.ORDER = {
            "id":null,
            "registerdate":null,
            "deliverdate":null,
            "receptiondate":null,
            "description":null,
            "status":null
        };
        this._route.params.subscribe(
            params =>{
                this.id = params["id"] ;
                console.log(this.id);
                this.getOrderdetails(this.id);
                console.log(params);
            }
        );
        this.getSeccion();
    }

    asignOrder(){
        this.asignorder = new Asignorder(this.id,this.asignList);
        this.onSubmit();
    }

    onSubmit(){
        this._orderdetailsService.asignComplete(this.asignorder).subscribe(
            response => {
                this.status = response.status;
                console.log(response);
                if(this.status != "success"){
                    this.status = "error";
                }else{
                    //this.getOrder();
                    this.getOrderdetails(this.id);
                    alert(response.msg);
                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }
    asignModal(rawmatter,categorycode,seccionid,quantity){
        console.log(rawmatter,categorycode,seccionid,quantity);
        // this.asignList.push({'code':categorycode,'rawmatter':rawmatter,'seccionid':seccionid,'quantity':quantity});
        // this.quantity = 1;
        if(this.asignList.length == 0){
            this.quantity = this.asignquantity;
            this.asignList.push({'code':categorycode,'rawmatter':rawmatter,'seccionid':seccionid,'quantity':quantity});
            this.quantity = 1;
        }else{
            //detailsList
            this.rawmatterquantity;
            var acu = 0;
            for (var j = 0; j < this.asignList.length; j++) {
                if(this.asignList[j].rawmatter == rawmatter){
                    acu = acu + this.asignList[j].quantity;
                    console.log("Acumulador" + acu );
                }
            }
            console.log("Acumulador" + (acu + this.asignquantity) + "  >= " + this.rawmatterquantity);
            if ((acu + this.asignquantity) > this.rawmatterquantity) {
                console.log("se paso");
                alert("Cantidad Exedida");
                acu = 0;
                return;
            }
            for(var i = 0; i < this.asignList.length;i++){
                console.log(i);
                if(this.asignList[i].code == categorycode && this.asignList[i].seccionid == seccionid){
                    this.asignList[i].quantity = this.asignList[i].quantity + this.asignquantity;
                    return;
                }
            }
            this.quantity = this.asignquantity;
            this.asignList.push({'code':categorycode,'rawmatter':rawmatter,'seccionid':seccionid,'quantity':this.asignquantity});
            this.quantity = 1;
        }
        console.log(this.asignList);
    }
    modalAsignOpen(code,name,quantity){
        this.rawmattercode = code;
        this.rawmattername = name;
        this.rawmatterquantity = quantity;
        console.log("AsignOpen"+this.rawmattercode,this.rawmattername,this.rawmatterquantity);
        this.getCategoryInfo(code);
    }
    modalOpen(id,quantity,code,name){
        console.log(id);
        this.RAW.id       = id;
        this.RAW.quantity = quantity;
        this.RAW.code     = code;
        this.RAW.name     = name;
        console.log(this.RAW);
    }

    createOrder(){
        // this.orders = new Orders(0,supplierid,1,null,deliverdate,null,descriprion,this.details);
        // console.log("this.orders");
        // console.log(this.orders);
        console.log(this.asignList);
        this.onSubmit();
    }

    editMaterial() {
        this._orderdetailsService.updateMatter(this.RAW).subscribe(
            response => {
                this.status = response.status;
                if (response.status = "success") {
                    //this.detailsList = response.detail;
                    console.log("Response Update");
                    console.log(response);
                    this.getOrderdetails(this.id);
                } else {
                    alert("ERROR");
                }
            },
            error => {
                this.errorMessage = <any>error;
                if (this.errorMessage != null) {
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );

    }
    getOrderdetails(id) {
        this._orderdetailsService.getDetail(id).subscribe(
            response => {
                this.status = response.status;
                if (response.status = "succes") {
                    this.detailsList = response.details;
                    this.orderdata = response.order;
                    console.log(this.detailsList);
                    console.log(this.orderdata);
                    this.ORDER.id = response.order.id;
                    this.ORDER.registerdate = response.order.registerdate.timestamp;
                    this.ORDER.deliverdate = response.order.deliverdate.timestamp;
                    this.ORDER.description = response.order.description;
                    this.ORDER.status = response.order.status;
                } else {
                    this.detailsList = response;
                }
            },
            error => {
                this.errorMessage = <any>error;
                if (this.errorMessage != null) {
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );
    }
    getSeccion(){
        this._orderdetailsService.getSeccion().subscribe(
            response =>{
                this.seccionList = response;
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );
    }
    getCategoryInfo(code){
        console.log(code);
        this._orderdetailsService.getCategoryInfo(code).subscribe(
            response =>{

                this.status= response.status;
                console.log(response);
                if(this.status!= "success"){
                    //this.status= "error";
                    alert(response.msg)
                }else{
                    this.categoryList  = response.data;
                }
                // this.categoryList = response;
                // console.log(this.categoryList   )
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );
    }

}