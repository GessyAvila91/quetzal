"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by getsemaniavila on 12/19/16.
 */
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var seccion_service_1 = require('../services/seccion.service');
var SeccioncontentComponent = (function () {
    function SeccioncontentComponent(_route, _router, _seccionService) {
        this._route = _route;
        this._router = _router;
        this._seccionService = _seccionService;
        this.title = "Seccion";
        this.nombre = "{{seccion.id}}";
        this.status = "";
    }
    SeccioncontentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._route.params.subscribe(function (params) {
            _this.id = +params["id"];
            _this.getInfo(_this.id);
            //this.getSeccion(this.id);
        });
        this.CoS = {
            "idr": "",
            "code": "",
            "cname": "",
            "rcode": "",
            "rmname": "",
            "quantity": 0,
            "id": this.id
        };
        this.getCategory();
    };
    SeccioncontentComponent.prototype.modalAddOpen = function (rcode, rmname, code, cname) {
        this.CoS.code = code;
        this.CoS.cname = cname;
        this.CoS.rcode = rcode;
        this.CoS.rmname = rmname;
    };
    SeccioncontentComponent.prototype.modalEditOpen = function (id, quantity, ccode, cname, cmatter) {
        this.CoS.idr = id;
        this.CoS.quantity = quantity;
        this.CoS.code = ccode;
        this.CoS.rcode = cmatter;
        this.CoS.cname = cname;
    };
    SeccioncontentComponent.prototype.addProduct = function (quantity) {
        this.CoS.quantity = quantity;
        this.CoS.id = this.id;
        console.log(this.CoS);
        this.addSubmit();
    };
    SeccioncontentComponent.prototype.editProduct = function (quantity) {
        this.CoS.quantity = quantity;
        this.CoS.id = this.id;
        console.log(this.CoS);
        this.editSubmit();
    };
    SeccioncontentComponent.prototype.editSubmit = function () {
        var _this = this;
        console.log(this.CoS);
        this._seccionService.edit(this.CoS).subscribe(function (response) {
            _this.status = response.status;
            if (_this.status == "success") {
                alert(response.msg);
                console.log(response);
                _this.getInfo(_this.id);
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    SeccioncontentComponent.prototype.addSubmit = function () {
        var _this = this;
        console.log(this.CoS);
        this._seccionService.add(this.CoS).subscribe(function (response) {
            _this.status = response.status;
            if (_this.status == "success") {
                alert("Producto Agregado");
                console.log(response);
                _this.getInfo(_this.id);
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    SeccioncontentComponent.prototype.getInfo = function (id) {
        var _this = this;
        this._seccionService.getInfo(id).subscribe(function (response) {
            _this.status = response.status;
            if (_this.status == "success") {
                //this.infoList = response.data;
                //console.log("DaTa"+JSON.stringify(response.data));
                _this.title = "Seccion: " + response.seccion.name;
                _this.categoryList = response.data;
            }
            else {
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición" + _this.errorMessage);
            }
        });
    };
    SeccioncontentComponent.prototype.getCategory = function () {
        var _this = this;
        this._seccionService.getCategoryList().subscribe(function (response) {
            /*
             this.status= response.status;
             console.log(response);
             if(this.status!= "success"){
             this.status= "error";
             }else{
             this.categoryList = response.data;
             }*/
            console.log("CategoryList" + response);
            _this.rawwmatterList = response;
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    SeccioncontentComponent = __decorate([
        core_1.Component({
            selector: 'seccioncontent',
            templateUrl: 'app/view/seccioncontent.html',
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [seccion_service_1.SeccionService]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, seccion_service_1.SeccionService])
    ], SeccioncontentComponent);
    return SeccioncontentComponent;
}());
exports.SeccioncontentComponent = SeccioncontentComponent;
//# sourceMappingURL=seccioncontent.component.js.map