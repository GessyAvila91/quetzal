/**
 * Created by GessyAvila on 28-Jul-17.
 */
/**
 * Created by getsemaniavila on 12/19/16.
 */
import {Component,OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";
import {SeccionService} from '../services/seccion.service';
import {Seccion} from "../model/seccion";

@Component({
    selector: 'seccion',
    templateUrl: 'app/view/seccion.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [SeccionService]
})

export class SeccionComponent{
    public titulo="Seccion";
    public seccion:Seccion;
    public status;
    public errorMessage;
    public seccionList;
    public productList;

    constructor(
        private _seccionService: SeccionService,
        private _route: ActivatedRoute,
        private _router:Router
    ){}

    ngOnInit(){
        this.seccion = new Seccion(null,null,null);
        this.getSeccion();
    }
    reset(){
        this.seccion = new Seccion(null,null,null);
    }
    onSubmit(){
        console.log(this.seccion );
        this._seccionService.new(this.seccion).subscribe(
            response => {
                this.status = response.status;
                console.log(response);
                if(this.status == "success"){
                    this.status = "success";
                    alert(response.msg);
                    this.getSeccion();
                    this.reset();
                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }

    getSeccion(){
        this._seccionService.getSeccion().subscribe(
            response =>{
                /*
                 this.status= response.status;
                 console.log(response);
                 if(this.status!= "success"){
                 this.status= "error";
                 }else{
                 this.productList = response.data;
                 }*/
                this.seccionList = response;
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );
    }

}