"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by getsemaniavila on 12/19/16.
 */
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var product_service_1 = require('../services/product.service');
var product_1 = require('../model/product');
var ProductComponent = (function () {
    function ProductComponent(_productService, _route, _router) {
        this._productService = _productService;
        this._route = _route;
        this._router = _router;
        this.titulo = "Materia Prima";
    }
    ProductComponent.prototype.ngOnInit = function () {
        this.product = new product_1.Product(1, 1, "1", "1", 1, 1, 1);
        //console.log(this.product);
        //Conseguir todos los productos
        console.log(this.getProduct());
        this.getProduct();
    };
    ProductComponent.prototype.onSubmit = function () {
        var _this = this;
        console.log(this.product);
        this._productService.new(this.product).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status != "success") {
                _this.status = "error";
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    ProductComponent.prototype.getProduct = function () {
        var _this = this;
        this._productService.getProduct().subscribe(function (response) {
            /*
            this.status= response.status;
            console.log(response);
            if(this.status!= "success"){
                this.status= "error";
            }else{
                this.productList = response.data;
            }*/
            _this.productList = response;
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    ProductComponent = __decorate([
        core_1.Component({
            selector: 'product',
            templateUrl: 'app/view/product.html',
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [product_service_1.ProductService]
        }), 
        __metadata('design:paramtypes', [product_service_1.ProductService, router_1.ActivatedRoute, router_1.Router])
    ], ProductComponent);
    return ProductComponent;
}());
exports.ProductComponent = ProductComponent;
//# sourceMappingURL=product.component.js.map