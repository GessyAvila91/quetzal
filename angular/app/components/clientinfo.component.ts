/**
 * Created by getsemaniavila on 12/27/16.
 */
import {Component,OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";
import {ClientService} from '../services/client.service';
import {Client} from "../model/client";
import {Email} from "../model/email";
import {Phone} from "../model/phone";
import {Card} from "../model/card";

@Component({
    selector: 'Cliente',
    templateUrl: 'app/view/clientinfo.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [ClientService]
})

export class ClientInfoComponent{
    public titulo="Informacion del Cliente:";
    public status;
    public errorMessage;

    public id;
    public client:Client;
    public phone:Phone;
    public email:Email;
    public correocorreo;
    public descripcioncorreo;

    public card:Card;
    
    public phoneList;
    public emailList;
    public cardList;

    constructor(
        private _clientService: ClientService,
        private _route: ActivatedRoute,
        private _router:Router
    ){}

    ngOnInit(){
        this.client = new Client(0,"","","","",
            "","","","","",
            "","","","");
        // this.phone = new Phone(null,null,null,null);
        // this.email = new Email(null,null,null,null);
        // this.card  = new Card(null,null,null,null);

        this._route.params.subscribe(
            params =>{
                this.id = params["id"] ;
                this.phone = new Phone(null,this.id,null,null);
                this.email = new Email(null,this.id,null,null);
                this.card  = new Card(null,this.id,null,null);
                console.log(this.id)
                this.getClientInfo(this.id);
            }

        );
    }
    reset(){
        this.phone = new Phone(null,this.id,null,null);
        this.email = new Email(null,this.id,null,null);
        this.card  = new  Card(null,this.id,null,null);
    }

    modalEditPhone(id,number,description){
        this.phone.id = id;
        this.phone.number = number;
        this.phone.description = description;
    }
    modalEditEmail(id,email,description){
        this.correocorreo = email;this.descripcioncorreo = description
        this.email.id = id;
        this.email.email = email;
        this.email.description = description;
    }
    newEmail(){
        this.email.email = this.correocorreo;
        this.email.description = this.descripcioncorreo;
        console.log(this.email);
        this._clientService.newemail(this.email,this.id).subscribe(
            response => {
                this.status = response.status;
                console.log(response);
                if(this.status != "success"){
                    this.status = "error";
                }else{
                    this.reset()
                    this.getClientInfo(this.id);
                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }
    newPhone(){
        console.log(this.phone);
        this._clientService.newphone(this.phone,this.id).subscribe(
            response => {
                this.status = response.status;
                console.log(response);
                if(this.status != "success"){
                    this.status = "error";
                }else{
                    this.reset()
                    this.getClientInfo(this.id);
                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }
    editEmail(){
        this.email.email = this.correocorreo;
        this.email.description = this.descripcioncorreo;
        console.log(this.email);
        this._clientService.emailedit(this.email).subscribe(
            response => {
                this.status = response.status;
                console.log(response);
                if(this.status != "success"){
                    this.status = "error";
                }else{
                    this.reset()
                    this.getClientInfo(this.id);
                }
            },error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }
    editPhone(){
        console.log(this.phone);
        this._clientService.phoneedit(this.phone).subscribe(
            response => {
                this.status = response.status;
                console.log(response);
                if(this.status != "success"){
                    this.status = "error";
                }else{
                    this.reset()
                    this.getClientInfo(this.id);
                }
            },error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }

    editClient(){
        console.log(this.client);
        this._clientService.edit(this.client).subscribe(
            response => {
                this.status = response.status;
                console.log(response);
                if(this.status != "success"){
                    this.status = "error";
                }else{
                    this.reset()
                    this.getClientInfo(this.id);
                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }

    getClientInfo(id){
        this._clientService.getInfo(id).subscribe(
            response =>{
                 console.log(response);
                 this.status = response.status;
                 if(this.status != "success"){
                     this.status= "error";
                 }else {
                     this.client.id= response.data.id;

                     this.client.title = response.data.title;
                     this.client.name   = response.data.name;
                     this.client.lastnamepaternal = response.data.lastnamepaternal;
                     this.client.lastnamematernal = response.data.lastnamematernal;
                     this.client.rfc = response.data.rfc;

                     this.client.company = response.data.company;
                     this.client.billadress = response.data.billAdress;
                     this.client.colony = response.data.colony;
                     this.client.state = response.data.state;
                     this.client.city = response.data.city;
                     this.client.pc = response.data.pc;

                     this.client.commentary = response.data.commentary

                     this.phoneList     = response.phone;
                     this.emailList     = response.email;
                     this.cardList      = response.card;

                     console.log(this.phoneList);
                     console.log(this.emailList);
                     console.log(this.cardList);
                     this.titulo = "Informacion del Cliente: " + this.client.title + ". " + this.client.name
                 }
                //this.clientList = response;
                //console.log(this.clientList);
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );
    }
}