/**
 * Created by getsemaniavila on 12/19/16.
 */
import {Component, OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";
import {SupplierService} from '../services/supplier.service';
import {Supplier} from '../model/supplier';

@Component({
    selector: 'Supplier',
    templateUrl: 'app/view/supplier.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [SupplierService]
})

export class SupplierComponent{
    public titulo="Proovedores";
    public supplier:Supplier;
    public errorMessage;
    public status;
    public supplierList;

    public SUP;

    constructor(
        private _supplierService: SupplierService,
        private _route: ActivatedRoute,
        private _router:Router
    ){}

    ngOnInit(){
        this.supplier = new Supplier(null,null,null,null,null,
                                     null,null,null,null,null);

        this.SUP = {
            "id":"",
            "tradename":"",
            "businessname":"",
            "contactperson":"",
            "addres":"",

            "phone":"",
            "email":"",
            "webpage":"",
            "rfc":"",
            "description":""
        };
        this.getSupplier();
    }
    reset(){
        this.supplier = new Supplier(null,null,null,null,null,
            null,null,null,null,null);
        this.SUP = {
            "id":"",
            "tradename":"",
            "businessname":"",
            "contactperson":"",
            "addres":"",

            "phone":"",
            "email":"",
            "webpage":"",
            "rfc":"",
            "description":""
        };
    }
    supEdit(){
        console.log(this.SUP);
        this._supplierService.edit(this.SUP).subscribe(
            response => {
                this.status = response.status;
                console.log(response);
                if(this.status == "success"){
                    this.status = "success";
                    alert(response.msg);
                    this.getSupplier()
                    this.reset()
                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }
    modalOpen(id,tradename,businessname,contactperson,addres,phone,email,webpage,rfc,description){

        this.SUP.id = id;
        this.SUP.tradename = tradename;
        this.SUP.businessname = businessname;
        this.SUP.contactperson = contactperson;
        this.SUP.addres = addres;

        this.SUP.phone= phone;
        this.SUP.email = email;
        this.SUP.webpage = webpage;
        this.SUP.rfc = rfc;
        this.SUP.description = description;

    }

    onSubmit(){
        console.log(this.supplier);
        this._supplierService.new(this.supplier).subscribe(
            response => {
                this.status = response.status;
                console.log(response);
                if(this.status == "success"){
                    this.status = "success";
                    alert("Supplier created");
                    this.getSupplier();
                    this.reset()
                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }

    getSupplier(){
        this._supplierService.getSupplier().subscribe(
            response =>{
                /*
                 this.status= response.status;
                 console.log(response);
                 if(this.status!= "success"){
                 this.status= "error";
                 }else{
                 this.productList = response.data;
                 }*/
                this.supplierList = response;
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }
}