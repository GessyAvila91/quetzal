"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by getsemaniavila on 12/27/16.
 */
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var client_service_1 = require('../services/client.service');
var client_1 = require("../model/client");
var email_1 = require("../model/email");
var phone_1 = require("../model/phone");
var card_1 = require("../model/card");
var ClientInfoComponent = (function () {
    function ClientInfoComponent(_clientService, _route, _router) {
        this._clientService = _clientService;
        this._route = _route;
        this._router = _router;
        this.titulo = "Informacion del Cliente:";
    }
    ClientInfoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.client = new client_1.Client(0, "", "", "", "", "", "", "", "", "", "", "", "", "");
        // this.phone = new Phone(null,null,null,null);
        // this.email = new Email(null,null,null,null);
        // this.card  = new Card(null,null,null,null);
        this._route.params.subscribe(function (params) {
            _this.id = params["id"];
            _this.phone = new phone_1.Phone(null, _this.id, null, null);
            _this.email = new email_1.Email(null, _this.id, null, null);
            _this.card = new card_1.Card(null, _this.id, null, null);
            console.log(_this.id);
            _this.getClientInfo(_this.id);
        });
    };
    ClientInfoComponent.prototype.reset = function () {
        this.phone = new phone_1.Phone(null, this.id, null, null);
        this.email = new email_1.Email(null, this.id, null, null);
        this.card = new card_1.Card(null, this.id, null, null);
    };
    ClientInfoComponent.prototype.modalEditPhone = function (id, number, description) {
        this.phone.id = id;
        this.phone.number = number;
        this.phone.description = description;
    };
    ClientInfoComponent.prototype.modalEditEmail = function (id, email, description) {
        this.correocorreo = email;
        this.descripcioncorreo = description;
        this.email.id = id;
        this.email.email = email;
        this.email.description = description;
    };
    ClientInfoComponent.prototype.newEmail = function () {
        var _this = this;
        this.email.email = this.correocorreo;
        this.email.description = this.descripcioncorreo;
        console.log(this.email);
        this._clientService.newemail(this.email, this.id).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status != "success") {
                _this.status = "error";
            }
            else {
                _this.reset();
                _this.getClientInfo(_this.id);
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    ClientInfoComponent.prototype.newPhone = function () {
        var _this = this;
        console.log(this.phone);
        this._clientService.newphone(this.phone, this.id).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status != "success") {
                _this.status = "error";
            }
            else {
                _this.reset();
                _this.getClientInfo(_this.id);
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    ClientInfoComponent.prototype.editEmail = function () {
        var _this = this;
        this.email.email = this.correocorreo;
        this.email.description = this.descripcioncorreo;
        console.log(this.email);
        this._clientService.emailedit(this.email).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status != "success") {
                _this.status = "error";
            }
            else {
                _this.reset();
                _this.getClientInfo(_this.id);
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    ClientInfoComponent.prototype.editPhone = function () {
        var _this = this;
        console.log(this.phone);
        this._clientService.phoneedit(this.phone).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status != "success") {
                _this.status = "error";
            }
            else {
                _this.reset();
                _this.getClientInfo(_this.id);
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    ClientInfoComponent.prototype.editClient = function () {
        var _this = this;
        console.log(this.client);
        this._clientService.edit(this.client).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status != "success") {
                _this.status = "error";
            }
            else {
                _this.reset();
                _this.getClientInfo(_this.id);
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    ClientInfoComponent.prototype.getClientInfo = function (id) {
        var _this = this;
        this._clientService.getInfo(id).subscribe(function (response) {
            console.log(response);
            _this.status = response.status;
            if (_this.status != "success") {
                _this.status = "error";
            }
            else {
                _this.client.id = response.data.id;
                _this.client.title = response.data.title;
                _this.client.name = response.data.name;
                _this.client.lastnamepaternal = response.data.lastnamepaternal;
                _this.client.lastnamematernal = response.data.lastnamematernal;
                _this.client.rfc = response.data.rfc;
                _this.client.company = response.data.company;
                _this.client.billadress = response.data.billAdress;
                _this.client.colony = response.data.colony;
                _this.client.state = response.data.state;
                _this.client.city = response.data.city;
                _this.client.pc = response.data.pc;
                _this.client.commentary = response.data.commentary;
                _this.phoneList = response.phone;
                _this.emailList = response.email;
                _this.cardList = response.card;
                console.log(_this.phoneList);
                console.log(_this.emailList);
                console.log(_this.cardList);
                _this.titulo = "Informacion del Cliente: " + _this.client.title + ". " + _this.client.name;
            }
            //this.clientList = response;
            //console.log(this.clientList);
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    ClientInfoComponent = __decorate([
        core_1.Component({
            selector: 'Cliente',
            templateUrl: 'app/view/clientinfo.html',
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [client_service_1.ClientService]
        }), 
        __metadata('design:paramtypes', [client_service_1.ClientService, router_1.ActivatedRoute, router_1.Router])
    ], ClientInfoComponent);
    return ClientInfoComponent;
}());
exports.ClientInfoComponent = ClientInfoComponent;
//# sourceMappingURL=clientinfo.component.js.map