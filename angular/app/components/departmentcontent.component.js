"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by getsemaniavila on 12/19/16.
 */
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var department_service_1 = require('../services/department.service');
var DepartmentcontentComponent = (function () {
    function DepartmentcontentComponent(_route, _router, _departmentService) {
        this._route = _route;
        this._router = _router;
        this._departmentService = _departmentService;
        this.titulo = "Seccion";
        this.nombre = "{{departamento.id}}";
        this.status = "";
        this.productId = 0;
    }
    DepartmentcontentComponent.prototype.ngOnInit = function () {
        var _this = this;
        //this.department = new Department(100,"","");
        //this.productondepartament = Productondepartament(1,1,1,1);
        //this.getDepartment();
        //this.getInfo();
        this.pod = {
            "productoName": "nombre",
            "productoId": 0,
            "departamentoId": this.id,
            "cantidad": 0
        };
        this._route.params.subscribe(function (params) {
            _this.id = +params["id"];
            _this.getInfo(_this.id);
            //this.getDepartamento(this.id);
        });
        this.getProduct();
    };
    DepartmentcontentComponent.prototype.modalOpen = function (proid, name) {
        this.pod.productoId = proid;
        this.pod.productoName = name;
    };
    DepartmentcontentComponent.prototype.addProduct = function (cantidad) {
        this.pod.cantidad = cantidad;
        this.pod.departamentoId = this.id;
        this.onSubmit();
    };
    DepartmentcontentComponent.prototype.onSubmit = function () {
        var _this = this;
        console.log(this.pod);
        this._departmentService.add(this.pod).subscribe(function (response) {
            _this.status = response.status;
            if (_this.status == "success") {
                alert("Producto Agregado");
                console.log(response);
                _this.getInfo(_this.id);
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    DepartmentcontentComponent.prototype.getInfo = function (id) {
        var _this = this;
        this._departmentService.getInfo(id).subscribe(function (response) {
            _this.status = response.status;
            if (_this.status == "success") {
                _this.infoList = response.data;
                console.log("DaTa" + JSON.stringify(response.data));
            }
            else {
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición" + _this.errorMessage);
            }
        } /*,
        departamentoinf => {
            this.departmentList = departamentoinf.nombre;
            console.log("depName"+this.departmentList);
            if(this.status = "success"){

            }
        }*/ /*,
        departamentoinf => {
            this.departmentList = departamentoinf.nombre;
            console.log("depName"+this.departmentList);
            if(this.status = "success"){

            }
        }*/);
    };
    DepartmentcontentComponent.prototype.getProduct = function () {
        var _this = this;
        this._departmentService.getProductList().subscribe(function (response) {
            /*
             this.status= response.status;
             console.log(response);
             if(this.status!= "success"){
             this.status= "error";
             }else{
             this.productList = response.data;
             }*/
            console.log("ProductList" + response);
            _this.productList = response;
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    DepartmentcontentComponent.prototype.getDepartamento = function (id) {
        var _this = this;
        this._departmentService.getInfo(id).subscribe(function (response) {
            /*
             this.status= response.status;
             console.log(response);
             if(this.status!= "success"){
             this.status= "error";
             }else{
             this.productList = response.data;
             }*/
            _this.nombre = response;
            console.log("M2" + _this.nombre);
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    DepartmentcontentComponent = __decorate([
        core_1.Component({
            selector: 'departmentcontent',
            templateUrl: 'app/view/departmentcontent.html',
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [department_service_1.DepartmentService]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, department_service_1.DepartmentService])
    ], DepartmentcontentComponent);
    return DepartmentcontentComponent;
}());
exports.DepartmentcontentComponent = DepartmentcontentComponent;
//# sourceMappingURL=departmentcontent.component.js.map