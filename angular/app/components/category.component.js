"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by GessyAvila on 26-Jul-17.
 */
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var category_service_1 = require('../services/category.service');
var category_1 = require('../model/category');
var CategoryComponent = (function () {
    function CategoryComponent(_categoryService, _route, _router) {
        this._categoryService = _categoryService;
        this._route = _route;
        this._router = _router;
        this.titulo = "Category : de <<rawmmatter>>";
        this.code = "";
    }
    CategoryComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.category = new category_1.Category(null, null, null, null);
        this._route.params.subscribe(function (params) {
            _this.code = params["code"];
            _this.category = new category_1.Category(null, _this.code, null, null);
            console.log(_this.code);
            _this.getInfo(_this.code);
            console.log(params);
        });
    };
    CategoryComponent.prototype.sussess = function () {
        this.reset();
        this.getInfo(this.code);
    };
    CategoryComponent.prototype.reset = function () {
        this.category.code = null;
        this.category.name = null;
        this.category.rawmatterCode = null;
        this.category.description = null;
    };
    CategoryComponent.prototype.modalEdit = function (code, name, rawmatterCode, description, price, minreorder, maxstock) {
        this.category.code = code;
        this.category.name = name;
        this.category.rawmatterCode = rawmatterCode;
        this.category.description = description;
    };
    CategoryComponent.prototype.onSubmit = function () {
        var _this = this;
        console.log(this.category);
        this._categoryService.new(this.category).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status != "success") {
                _this.status = "error";
            }
            else {
                _this.sussess();
                alert(response.msg);
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    CategoryComponent.prototype.editcategory = function () {
    };
    CategoryComponent.prototype.getInfo = function (code) {
        var _this = this;
        console.log(code);
        this._categoryService.getInfo(code).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status != "success") {
                //this.status= "error";
                alert("Materia Prima no cuenta con categorías, favor de agregarlas \nServer Response" + response.msg);
                _this.titulo = "Categoria de: " + response.rawmatter.code + " / " + response.rawmatter.name;
            }
            else {
                _this.titulo = "Categoria de: " + response.rawmatter.code + " / " + response.rawmatter.name;
                _this.categoryList = response.data;
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    CategoryComponent.prototype.getCategory = function () {
        var _this = this;
        this._categoryService.getCategory().subscribe(function (response) {
            /*
             this.status= response.status;
             console.log(response);
             if(this.status!= "success"){
             this.status= "error";
             }else{
             this.productList = response.data;
             }*/
            _this.categoryList = response;
            console.log(_this.categoryList);
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    CategoryComponent = __decorate([
        core_1.Component({
            selector: 'category',
            templateUrl: 'app/view/category.html',
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [category_service_1.CategoryService]
        }), 
        __metadata('design:paramtypes', [category_service_1.CategoryService, router_1.ActivatedRoute, router_1.Router])
    ], CategoryComponent);
    return CategoryComponent;
}());
exports.CategoryComponent = CategoryComponent;
//# sourceMappingURL=category.component.js.map