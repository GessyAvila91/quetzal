"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by getsemaniavila on 12/19/16.
 */
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var rawmatter_service_1 = require('../services/rawmatter.service');
var rawmatter_1 = require('../model/rawmatter');
var RawmatterComponent = (function () {
    function RawmatterComponent(_rawmatterService, _route, _router) {
        this._rawmatterService = _rawmatterService;
        this._route = _route;
        this._router = _router;
        this.titulo = "Materia Prima";
    }
    RawmatterComponent.prototype.ngOnInit = function () {
        this.rawmatter = new rawmatter_1.Rawmatter(null, null, null, null, null, null);
        this.getRawmatter();
    };
    RawmatterComponent.prototype.reset = function () {
        this.rawmatter.code = null;
        this.rawmatter.name = null;
        this.rawmatter.description = null;
        this.rawmatter.price = null;
        this.rawmatter.minReorder = null;
        this.rawmatter.maxStock = null;
    };
    RawmatterComponent.prototype.modalEdit = function (code, name, description, price, minreorder, maxstock) {
        this.rawmatter.code = code;
        this.rawmatter.name = name;
        this.rawmatter.description = description;
        this.rawmatter.price = price;
        this.rawmatter.minReorder = minreorder;
        this.rawmatter.maxStock = maxstock;
    };
    RawmatterComponent.prototype.onSubmit = function () {
        var _this = this;
        console.log(this.rawmatter);
        this._rawmatterService.new(this.rawmatter).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status != "success") {
                _this.status = "error";
            }
            else {
                _this.reset();
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    RawmatterComponent.prototype.editRawmatter = function () {
        var _this = this;
        console.log(this.rawmatter);
        this._rawmatterService.updateRawmatter(this.rawmatter).subscribe(function (response) {
            _this.status = response.status;
            if (_this.status == "success") {
                _this.getRawmatter();
            }
            else {
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    RawmatterComponent.prototype.getRawmatter = function () {
        var _this = this;
        this._rawmatterService.getRawmatter().subscribe(function (response) {
            _this.rawmatterList = response;
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    RawmatterComponent.prototype.searchRawmatter = function () {
        var _this = this;
        this._rawmatterService.searchRawmatter(this.rawmatter).subscribe(function (response) {
            _this.status = response.status;
            if (_this.status == "success") {
                console.log("successs");
                _this.rawmatterList = response.data;
                console.log(_this.rawmatter);
            }
            else {
                alert("Error en la petición");
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    RawmatterComponent = __decorate([
        core_1.Component({
            selector: 'rawmatter',
            templateUrl: 'app/view/rawmatter.html',
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [rawmatter_service_1.RawmatterServices],
        }), 
        __metadata('design:paramtypes', [rawmatter_service_1.RawmatterServices, router_1.ActivatedRoute, router_1.Router])
    ], RawmatterComponent);
    return RawmatterComponent;
}());
exports.RawmatterComponent = RawmatterComponent;
//# sourceMappingURL=rawmatter.component.js.map