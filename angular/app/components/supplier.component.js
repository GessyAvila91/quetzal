"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by getsemaniavila on 12/19/16.
 */
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var supplier_service_1 = require('../services/supplier.service');
var supplier_1 = require('../model/supplier');
var SupplierComponent = (function () {
    function SupplierComponent(_supplierService, _route, _router) {
        this._supplierService = _supplierService;
        this._route = _route;
        this._router = _router;
        this.titulo = "Proovedores";
    }
    SupplierComponent.prototype.ngOnInit = function () {
        this.supplier = new supplier_1.Supplier(null, null, null, null, null, null, null, null, null, null);
        this.SUP = {
            "id": "",
            "tradename": "",
            "businessname": "",
            "contactperson": "",
            "addres": "",
            "phone": "",
            "email": "",
            "webpage": "",
            "rfc": "",
            "description": ""
        };
        this.getSupplier();
    };
    SupplierComponent.prototype.reset = function () {
        this.supplier = new supplier_1.Supplier(null, null, null, null, null, null, null, null, null, null);
        this.SUP = {
            "id": "",
            "tradename": "",
            "businessname": "",
            "contactperson": "",
            "addres": "",
            "phone": "",
            "email": "",
            "webpage": "",
            "rfc": "",
            "description": ""
        };
    };
    SupplierComponent.prototype.supEdit = function () {
        var _this = this;
        console.log(this.SUP);
        this._supplierService.edit(this.SUP).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status == "success") {
                _this.status = "success";
                alert(response.msg);
                _this.getSupplier();
                _this.reset();
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    SupplierComponent.prototype.modalOpen = function (id, tradename, businessname, contactperson, addres, phone, email, webpage, rfc, description) {
        this.SUP.id = id;
        this.SUP.tradename = tradename;
        this.SUP.businessname = businessname;
        this.SUP.contactperson = contactperson;
        this.SUP.addres = addres;
        this.SUP.phone = phone;
        this.SUP.email = email;
        this.SUP.webpage = webpage;
        this.SUP.rfc = rfc;
        this.SUP.description = description;
    };
    SupplierComponent.prototype.onSubmit = function () {
        var _this = this;
        console.log(this.supplier);
        this._supplierService.new(this.supplier).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status == "success") {
                _this.status = "success";
                alert("Supplier created");
                _this.getSupplier();
                _this.reset();
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    SupplierComponent.prototype.getSupplier = function () {
        var _this = this;
        this._supplierService.getSupplier().subscribe(function (response) {
            /*
             this.status= response.status;
             console.log(response);
             if(this.status!= "success"){
             this.status= "error";
             }else{
             this.productList = response.data;
             }*/
            _this.supplierList = response;
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    SupplierComponent = __decorate([
        core_1.Component({
            selector: 'Supplier',
            templateUrl: 'app/view/supplier.html',
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [supplier_service_1.SupplierService]
        }), 
        __metadata('design:paramtypes', [supplier_service_1.SupplierService, router_1.ActivatedRoute, router_1.Router])
    ], SupplierComponent);
    return SupplierComponent;
}());
exports.SupplierComponent = SupplierComponent;
//# sourceMappingURL=supplier.component.js.map