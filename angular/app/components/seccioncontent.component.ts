/**
 * Created by getsemaniavila on 12/19/16.
 */
import {Component,OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";
import {SeccionService} from '../services/seccion.service';
import {Seccion} from "../model/seccion";
import {Rawmatter} from "../model/rawmatter";


@Component({
    selector: 'seccioncontent',
    templateUrl: 'app/view/seccioncontent.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [SeccionService]
})

export class SeccioncontentComponent{
    public title="Seccion";
    public nombre="{{seccion.id}}";

    public CoS;

    public status = "";
    public errorMessage;
    public seccionList;
    public infoList;
    public categoryList;
    public rawwmatterList;
    public id;

    constructor(
        private _route: ActivatedRoute,
        private _router:Router,
        private _seccionService: SeccionService
    ){}



    ngOnInit(){
        this._route.params.subscribe(
            params =>{
                this.id = +params["id"];
                this.getInfo(this.id);
                //this.getSeccion(this.id);
            }
        );
        this.CoS = {
            "idr":"",
            "code":"",
            "cname":"",
            "rcode":"",
            "rmname":"",
            "quantity": 0,
            "id": this.id
        };
        this.getCategory();
    }
    modalAddOpen(rcode,rmname,code,cname){
        this.CoS.code = code;
        this.CoS.cname = cname;
        this.CoS.rcode= rcode;
        this.CoS.rmname = rmname;
    }
    modalEditOpen(id,quantity,ccode,cname,cmatter){
        this.CoS.idr = id;
        this.CoS.quantity = quantity;
        this.CoS.code = ccode;
        this.CoS.rcode = cmatter;
        this.CoS.cname = cname;
    }
    addProduct(quantity){
        this.CoS.quantity = quantity;
        this.CoS.id= this.id;
        console.log(this.CoS);
        this.addSubmit();
    }
    editProduct(quantity){
        this.CoS.quantity = quantity;
        this.CoS.id= this.id;
        console.log(this.CoS);
        this.editSubmit();
    }

    editSubmit(){
        console.log(this.CoS);
        this._seccionService.edit(this.CoS).subscribe(
            response => {
                this.status = response.status;
                if(this.status == "success"){
                    alert(response.msg);
                    console.log(response);
                    this.getInfo(this.id);
                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }
    addSubmit(){
        console.log(this.CoS);
        this._seccionService.add(this.CoS).subscribe(
            response => {
                this.status = response.status;
                if(this.status == "success"){
                    alert("Producto Agregado");
                    console.log(response);
                    this.getInfo(this.id);
                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }
    getInfo(id){
        this._seccionService.getInfo(id).subscribe(
            response=>{
                this.status = response.status;
                if(this.status == "success"){
                    //this.infoList = response.data;
                    //console.log("DaTa"+JSON.stringify(response.data));
                    this.title = "Seccion: "+ response.seccion.name;
                    this.categoryList = response.data;
                }else{
                }
            },
            error=>{
                this.errorMessage = <any>error;
                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición"+this.errorMessage);
                }
            }
        );
    }
    getCategory(){
        this._seccionService.getCategoryList().subscribe(
            response =>{
                /*
                 this.status= response.status;
                 console.log(response);
                 if(this.status!= "success"){
                 this.status= "error";
                 }else{
                 this.categoryList = response.data;
                 }*/
                console.log("CategoryList"+response);
                this.rawwmatterList= response;
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );
    }


}