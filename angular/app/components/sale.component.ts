/**
 * Created by getsemaniavila on 12/19/16.
 */
import {Component,OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";
import {SaleService} from '../services/sale.service';
import {Department} from "../model/department";

@Component({
    selector: 'sale',
    templateUrl: 'app/view/sale.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [SaleService]
})

export class SaleComponent{
    public titulo="Ventas";
    public status;
    public errorMessage;
    public departmentList;

    constructor(
        private _saleService: SaleService,
        private _route: ActivatedRoute,
        private _router:Router
    ){}

}