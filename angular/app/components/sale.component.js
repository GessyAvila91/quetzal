"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by getsemaniavila on 12/19/16.
 */
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var sale_service_1 = require('../services/sale.service');
var SaleComponent = (function () {
    function SaleComponent(_saleService, _route, _router) {
        this._saleService = _saleService;
        this._route = _route;
        this._router = _router;
        this.titulo = "Ventas";
    }
    SaleComponent = __decorate([
        core_1.Component({
            selector: 'sale',
            templateUrl: 'app/view/sale.html',
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [sale_service_1.SaleService]
        }), 
        __metadata('design:paramtypes', [sale_service_1.SaleService, router_1.ActivatedRoute, router_1.Router])
    ], SaleComponent);
    return SaleComponent;
}());
exports.SaleComponent = SaleComponent;
//# sourceMappingURL=sale.component.js.map