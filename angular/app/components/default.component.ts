/**
 * Created by getsemaniavila on 12/19/16.
 */
import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";
import {LoginService} from '../services/login.service';
//hex

//hex
@Component({
    selector: 'default',
    templateUrl: 'app/view/default.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [LoginService]
})

export class DefaultComponent {
    //hex

    //hex
    public titulo="Default";
    public identity;
    public token

    constructor(
        private _loginService: LoginService
    ){}

    ngOnInit(){
        this.identity= this._loginService.getIdentity();
        this.token = this._loginService.getToken();
    }


}