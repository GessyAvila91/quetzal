/**
 * Created by Gessy on 20/04/2017.
 */
/**
 * Created by getsemaniavila on 12/19/16.
 */
import {Component,OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";
import {OrdersService} from '../services/orders.service';
import {RawmatterServices} from '../services/rawmatter.service';
import {Orders} from "../model/orders";
import {Rawmatter} from '../model/rawmatter';
import {stringify} from "@angular/platform-browser-dynamic/src/facade/lang";
import {count} from "rxjs/operator/count";



@Component({
    selector: 'orders',
    templateUrl: 'app/view/orders.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [OrdersService,RawmatterServices]
})

export class OrdersComponent{
    public titulo="Ordenes";
    public orders:Orders;
    public rawmatter:Rawmatter;
    public status;
    public errorMessage;
    public orderList;
    public supplierList;
    public rawmatterList;

    public request=[];
    public selector;

    public details=[];


    public quantity;
    public price;

    constructor(
        private _ordersService: OrdersService,
        private _rawmatterService: RawmatterServices,
        private _route: ActivatedRoute,
        private _router:Router
    ){}

    ngOnInit(){
        this.orders = new Orders(null,null,null,null,null,null,null,null);
        this.rawmatter = new Rawmatter(null,null,null,null,null,null);
        this.quantity = 1;
        this.quantity = 1.0;
        this.getAll();
    }

    editList(quantity,price,code){
        if (quantity == 0) {
            for (var i = 0; i < this.details.length; i++) {
                if (this.details[i].code == code) {
                    console.log(code);
                    var index = i;
                    this.details.splice(index,1);
                }
            }
        } else {
            for (var i = 0; i < this.details.length; i++) {
                if (this.details[i].code == this.rawmatter.code) {
                    this.details[i].quantity = quantity;
                    this.details[i].price    = price;
                }
            }
        }
    }

    addToList(quantity,price){
        if(this.details.length == 0){
            this.quantity = quantity;
            this.price = price;
            this.details.push({'code':this.rawmatter.code,'name':this.rawmatter.name,'quantity':this.quantity,'price':this.price});
        }else{
            for(var i = 0; i < this.details.length;i++){
                console.log(i);
                if(this.details[i].code == this.rawmatter.code){
                    this.details[i].quantity = this.details[i].quantity + quantity;
                    this.details[i].price    = price;
                    return;
                }
            }
            this.quantity = quantity;
            this.details.push({'code':this.rawmatter.code,'name':this.rawmatter.name,'quantity':this.quantity,'price':this.price});
        }
        console.log(this.rawmatter);
    }

    modalAdd(code,name,description,price,minreorder,maxstock){
        this.rawmatter.code = code;
        this.rawmatter.name = name;
        this.rawmatter.description = description;

        this.rawmatter.price = price;
        this.rawmatter.minReorder = minreorder;
        this.rawmatter.maxStock = maxstock;
    }

    createOrder(supplierid,deliverdate,descriprion){
        this.orders = new Orders(0,supplierid,1,null,deliverdate,null,descriprion,this.details);
        console.log("this.orders");
        console.log(this.orders);
        this.onSubmit();
    }

    onSubmit(){
        this._ordersService.new(this.orders).subscribe(
            response => {
                this.status = response.status;
                console.log(response);
                if(this.status != "success"){
                    this.status = "error";
                }else{
                    this.getOrder();
                    alert(response.msg);
                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }

    getAll(){
        this._ordersService.getAll().subscribe(
            response =>{
                console.log(response);
                this.orderList     = response.orders;
                this.supplierList  = response.supplier;
                this.rawmatterList = response.rawmatter;
                console.log(this.orderList);
                console.log(this.rawmatterList);
                console.log(this.supplierList);
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );
    }
    getOrder(){
        this._ordersService.getOrder().subscribe(
            response =>{
                this.orderList = response;
                console.log(response);
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );
    }
    getRawmatter(){
        this._ordersService.getRawmatter().subscribe(
            response =>{
                this.rawmatterList = response;
                console.log(response);
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );
    }
    searchRawmatter(){
        this._rawmatterService.searchRawmatter(this.rawmatter).subscribe(
            response =>{
                this.status = response.status;
                if(this.status == "success"){
                    console.log("successs");
                    this.rawmatterList = response.data;
                    console.log(this.rawmatter);
                }else{
                    alert("Error en la petición");
                }
            },
            error=>{
                this.errorMessage = <any>error;
                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }
    getSupplier(){
        this._ordersService.getSupplier().subscribe(
            response =>{
                this.supplierList = response;
                console.log(response);
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );
    }

}