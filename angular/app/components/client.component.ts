/**
 * Created by getsemaniavila on 12/27/16.
 */
import {Component,OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";
import {ClientService} from '../services/client.service';
import {Client} from "../model/client";

@Component({
    selector: 'client',
    templateUrl: 'app/view/client.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [ClientService]
})

export class ClientComponent{
    public titulo="Agenda de Clientes";
    public status;
    public client:Client;
    public errorMessage;
    public clientList;

    constructor(
        private _clientService: ClientService,
        private _route: ActivatedRoute,
        private _router:Router
    ){}

    ngOnInit(){
        this.client = new Client(0,"","","","",
                                "","","","","",
                                "","","","");
        this.getClient();
    }
    reset(){
        this.client.id= null;
        this.client.name = null;
        this.client.title = null;
        this.client.lastnamepaternal = null;
        this.client.lastnamematernal = null;

        this.client.category = null;
        this.client.rfc = null;
        this.client.company = null;
        this.client.billadress = null;
        this.client.colony = null;

        this.client.state = null;
        this.client.city = null;
        this.client.pc = null;
        this.client.commentary = null;
    }
    modalEdit(name,title,lastnamepaternal,lastnamematernal,category,rfc,company,billadress,colony,state,city,pc,commentary){
        this.client.title = title;
        this.client.name = name;
        this.client.lastnamepaternal =lastnamepaternal;
        this.client.lastnamematernal = lastnamematernal;
        this.client.category = category;

        this.client.rfc = rfc;
        this.client.company = company;
        this.client.billadress = billadress;
        this.client.colony = colony;
        this.client.state = state;

        this.client.city = city;
        this.client.pc = pc;
        this.client.commentary = commentary;
    }
    onSubmit(){
        console.log(this.client);
        this._clientService.new(this.client).subscribe(
            response => {
                this.status = response.status;
                console.log(response);
                if(this.status != "success"){
                    this.status = "error";
                }else{
                    this.reset();
                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }

        );
    }
    editClient(){

    }
    getClient(){
        this._clientService.getClient().subscribe(
            response =>{
                /*
                 this.status= response.status;
                 console.log(response);
                 if(this.status!= "success"){
                 this.status= "error";
                 }else{
                 this.productList = response.data;
                 }*/
                this.clientList = response;
                console.log(this.clientList);
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );
    }
}