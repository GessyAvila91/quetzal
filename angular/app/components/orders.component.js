"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by Gessy on 20/04/2017.
 */
/**
 * Created by getsemaniavila on 12/19/16.
 */
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var orders_service_1 = require('../services/orders.service');
var rawmatter_service_1 = require('../services/rawmatter.service');
var orders_1 = require("../model/orders");
var rawmatter_1 = require('../model/rawmatter');
var OrdersComponent = (function () {
    function OrdersComponent(_ordersService, _rawmatterService, _route, _router) {
        this._ordersService = _ordersService;
        this._rawmatterService = _rawmatterService;
        this._route = _route;
        this._router = _router;
        this.titulo = "Ordenes";
        this.request = [];
        this.details = [];
    }
    OrdersComponent.prototype.ngOnInit = function () {
        this.orders = new orders_1.Orders(null, null, null, null, null, null, null, null);
        this.rawmatter = new rawmatter_1.Rawmatter(null, null, null, null, null, null);
        this.quantity = 1;
        this.quantity = 1.0;
        this.getAll();
    };
    OrdersComponent.prototype.editList = function (quantity, price, code) {
        if (quantity == 0) {
            for (var i = 0; i < this.details.length; i++) {
                if (this.details[i].code == code) {
                    console.log(code);
                    var index = i;
                    this.details.splice(index, 1);
                }
            }
        }
        else {
            for (var i = 0; i < this.details.length; i++) {
                if (this.details[i].code == this.rawmatter.code) {
                    this.details[i].quantity = quantity;
                    this.details[i].price = price;
                }
            }
        }
    };
    OrdersComponent.prototype.addToList = function (quantity, price) {
        if (this.details.length == 0) {
            this.quantity = quantity;
            this.price = price;
            this.details.push({ 'code': this.rawmatter.code, 'name': this.rawmatter.name, 'quantity': this.quantity, 'price': this.price });
        }
        else {
            for (var i = 0; i < this.details.length; i++) {
                console.log(i);
                if (this.details[i].code == this.rawmatter.code) {
                    this.details[i].quantity = this.details[i].quantity + quantity;
                    this.details[i].price = price;
                    return;
                }
            }
            this.quantity = quantity;
            this.details.push({ 'code': this.rawmatter.code, 'name': this.rawmatter.name, 'quantity': this.quantity, 'price': this.price });
        }
        console.log(this.rawmatter);
    };
    OrdersComponent.prototype.modalAdd = function (code, name, description, price, minreorder, maxstock) {
        this.rawmatter.code = code;
        this.rawmatter.name = name;
        this.rawmatter.description = description;
        this.rawmatter.price = price;
        this.rawmatter.minReorder = minreorder;
        this.rawmatter.maxStock = maxstock;
    };
    OrdersComponent.prototype.createOrder = function (supplierid, deliverdate, descriprion) {
        this.orders = new orders_1.Orders(0, supplierid, 1, null, deliverdate, null, descriprion, this.details);
        console.log("this.orders");
        console.log(this.orders);
        this.onSubmit();
    };
    OrdersComponent.prototype.onSubmit = function () {
        var _this = this;
        this._ordersService.new(this.orders).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status != "success") {
                _this.status = "error";
            }
            else {
                _this.getOrder();
                alert(response.msg);
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    OrdersComponent.prototype.getAll = function () {
        var _this = this;
        this._ordersService.getAll().subscribe(function (response) {
            console.log(response);
            _this.orderList = response.orders;
            _this.supplierList = response.supplier;
            _this.rawmatterList = response.rawmatter;
            console.log(_this.orderList);
            console.log(_this.rawmatterList);
            console.log(_this.supplierList);
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    OrdersComponent.prototype.getOrder = function () {
        var _this = this;
        this._ordersService.getOrder().subscribe(function (response) {
            _this.orderList = response;
            console.log(response);
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    OrdersComponent.prototype.getRawmatter = function () {
        var _this = this;
        this._ordersService.getRawmatter().subscribe(function (response) {
            _this.rawmatterList = response;
            console.log(response);
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    OrdersComponent.prototype.searchRawmatter = function () {
        var _this = this;
        this._rawmatterService.searchRawmatter(this.rawmatter).subscribe(function (response) {
            _this.status = response.status;
            if (_this.status == "success") {
                console.log("successs");
                _this.rawmatterList = response.data;
                console.log(_this.rawmatter);
            }
            else {
                alert("Error en la petición");
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    OrdersComponent.prototype.getSupplier = function () {
        var _this = this;
        this._ordersService.getSupplier().subscribe(function (response) {
            _this.supplierList = response;
            console.log(response);
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    OrdersComponent = __decorate([
        core_1.Component({
            selector: 'orders',
            templateUrl: 'app/view/orders.html',
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [orders_service_1.OrdersService, rawmatter_service_1.RawmatterServices]
        }), 
        __metadata('design:paramtypes', [orders_service_1.OrdersService, rawmatter_service_1.RawmatterServices, router_1.ActivatedRoute, router_1.Router])
    ], OrdersComponent);
    return OrdersComponent;
}());
exports.OrdersComponent = OrdersComponent;
//# sourceMappingURL=orders.component.js.map