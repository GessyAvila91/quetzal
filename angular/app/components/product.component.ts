/**
 * Created by getsemaniavila on 12/19/16.
 */
import {Component, OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";
import {ProductService} from '../services/product.service';
import {Product} from '../model/product';

@Component({
    selector: 'product',
    templateUrl: 'app/view/product.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [ProductService]
})

export class ProductComponent{
    public titulo="Materia Prima";
    public product:Product;
    public errorMessage;
    public status;

    public productList;

    constructor(
        private _productService: ProductService,
        private _route: ActivatedRoute,
        private _router:Router
    ){}

    ngOnInit(){
        this.product = new Product(1,1,"1","1",1,1,1);

        //console.log(this.product);


        //Conseguir todos los productos
        console.log(this.getProduct());
        this.getProduct();
    }

    onSubmit(){
        console.log(this.product);
        this._productService.new(this.product).subscribe(
            response => {
                this.status = response.status;
                console.log(response);
                if(this.status != "success"){
                    this.status = "error";
                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }

        );
    }
    getProduct(){
        this._productService.getProduct().subscribe(

            response =>{
                /*
                this.status= response.status;
                console.log(response);
                if(this.status!= "success"){
                    this.status= "error";
                }else{
                    this.productList = response.data;
                }*/
                this.productList = response;
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }
}