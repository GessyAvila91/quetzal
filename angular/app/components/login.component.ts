/**
 * Created by getsemaniavila on 12/19/16.
 */
import {LoginService} from '../services/login.service';
import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";

@Component({
    selector: 'login',
    templateUrl: 'app/view/login.html',
    providers:[LoginService]
})

export class LoginComponent implements OnInit{
    public titulo: string = "Login";
    public user;
    public errorMessage;
    public token;
    public identity;
    constructor(
        private _loginService: LoginService,
        private _route: ActivatedRoute,
        private _router: Router
    ){}
    ngOnInit(){
        this._route.params.subscribe(params => {
            let logout = +params["id"];
            if(logout == 1){
                localStorage.removeItem("identity");
                localStorage.removeItem("token");
                this.identity = null;
                this.token = null;

                window.location.href = "/";
                this._router.navigate(["/index"]);
            }
        })

        this.user={
            "email": "",
            "password":"",
            "gethash":"false"
        };

        let ide = this._loginService.getIdentity();
        let tk = this._loginService.getToken();

        let identity =  this._loginService.getIdentity();

        console.log(ide);
        console.log(tk);

        if(identity != null && identity.sub){
            this._router.navigate(["/index"]);
        }
    }

    onSubmit(){
        console.log(this.user);
        this._loginService.signup(this.user).subscribe(
            response => {
                let identity = response;
                this.identity = identity;

                if(this.identity.length <= 1){
                    alert("Error en el servidor");
                }else{
                    if(!this.identity.status){
                        localStorage.setItem('identity', JSON.stringify(identity));

                        // GET TOKEN
                        this.user.gethash = "true";
                        this._loginService.signup(this.user).subscribe(
                            response => {
                                let token = response;
                                this.token = token;

                                if(this.token.length <= 0){
                                    alert("Error en el servidor");
                                }else{
                                    if(!this.token.status){
                                        localStorage.setItem('token', token);

                                        // REDIRECCION
                                        window.location.href = "/";
                                    }
                                }
                            },
                            error => {
                                this.errorMessage = <any>error;

                                if(this.errorMessage != null){
                                    console.log(this.errorMessage);
                                    alert("Error en la petición");
                                }
                            }

                        );
                    }
                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }

        );
    }
}
