/**
 * Created by Gessy on 18/01/2017.
 */
/**
 * Created by getsemaniavila on 12/19/16.
 */
import {Component,OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";
import {MerchService} from '../services/merch.service';

@Component({
    selector: 'merch',
    templateUrl: 'app/view/merch.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [MerchService]
})

export class MerchComponent{
    public titulo="Producto Final";
    public status;
    public errorMessage;
    public departmentList;

    constructor(
        private _merchService: MerchService,
        private _route: ActivatedRoute,
        private _router:Router
    ){}

    ngOnInit(){
        //this.merch = new Merch(1,"","");
    }

}