"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by getsemaniavila on 12/19/16.
 */
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var department_service_1 = require('../services/department.service');
var department_1 = require("../model/department");
var DepartmentComponent = (function () {
    function DepartmentComponent(_departmentService, _route, _router) {
        this._departmentService = _departmentService;
        this._route = _route;
        this._router = _router;
        this.titulo = "Seccion";
    }
    DepartmentComponent.prototype.ngOnInit = function () {
        this.department = new department_1.Department(0, "", "");
        this.getDepartment();
    };
    DepartmentComponent.prototype.onSubmit = function () {
        var _this = this;
        console.log(this.department);
        this._departmentService.new(this.department).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status == "success") {
                _this.status = "success";
                alert("Seccion creada");
                _this.getDepartment();
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    DepartmentComponent.prototype.getDepartment = function () {
        var _this = this;
        this._departmentService.getDepartment().subscribe(function (response) {
            /*
             this.status= response.status;
             console.log(response);
             if(this.status!= "success"){
             this.status= "error";
             }else{
             this.productList = response.data;
             }*/
            _this.departmentList = response;
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    DepartmentComponent = __decorate([
        core_1.Component({
            selector: 'department',
            templateUrl: 'app/view/department.html',
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [department_service_1.DepartmentService]
        }), 
        __metadata('design:paramtypes', [department_service_1.DepartmentService, router_1.ActivatedRoute, router_1.Router])
    ], DepartmentComponent);
    return DepartmentComponent;
}());
exports.DepartmentComponent = DepartmentComponent;
//# sourceMappingURL=department.component.js.map