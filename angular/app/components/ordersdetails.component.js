"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by Gessy on 20/04/2017.
 */
/**
 * Created by Gessy on 20/04/2017.
 */
/**
 * Created by getsemaniavila on 12/19/16.
 */
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var ordersdetails_service_1 = require('../services/ordersdetails.service');
var department_1 = require("../model/department");
var asignorder_1 = require("../model/asignorder");
var OrdersDetailsComponent = (function () {
    function OrdersDetailsComponent(_orderdetailsService, _route, _router) {
        this._orderdetailsService = _orderdetailsService;
        this._route = _route;
        this._router = _router;
        this.titulo = "Detalles de Ordenes";
        this.asignList = [];
        this.asignListAux = [];
        this.quantity = 0;
        this.rawmatterquantity = 0;
    }
    OrdersDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.asignorder = new asignorder_1.Asignorder(null, null);
        this.department = new department_1.Department(1, "", "");
        this.seccionid = 0;
        this.RAW = {
            "id": "",
            "quantity": "",
            "code": "",
            "name": ""
        };
        this.ORDER = {
            "id": null,
            "registerdate": null,
            "deliverdate": null,
            "receptiondate": null,
            "description": null,
            "status": null
        };
        this._route.params.subscribe(function (params) {
            _this.id = params["id"];
            console.log(_this.id);
            _this.getOrderdetails(_this.id);
            console.log(params);
        });
        this.getSeccion();
    };
    OrdersDetailsComponent.prototype.asignOrder = function () {
        this.asignorder = new asignorder_1.Asignorder(this.id, this.asignList);
        this.onSubmit();
    };
    OrdersDetailsComponent.prototype.onSubmit = function () {
        var _this = this;
        this._orderdetailsService.asignComplete(this.asignorder).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status != "success") {
                _this.status = "error";
            }
            else {
                //this.getOrder();
                _this.getOrderdetails(_this.id);
                alert(response.msg);
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    OrdersDetailsComponent.prototype.asignModal = function (rawmatter, categorycode, seccionid, quantity) {
        console.log(rawmatter, categorycode, seccionid, quantity);
        // this.asignList.push({'code':categorycode,'rawmatter':rawmatter,'seccionid':seccionid,'quantity':quantity});
        // this.quantity = 1;
        if (this.asignList.length == 0) {
            this.quantity = this.asignquantity;
            this.asignList.push({ 'code': categorycode, 'rawmatter': rawmatter, 'seccionid': seccionid, 'quantity': quantity });
            this.quantity = 1;
        }
        else {
            //detailsList
            this.rawmatterquantity;
            var acu = 0;
            for (var j = 0; j < this.asignList.length; j++) {
                if (this.asignList[j].rawmatter == rawmatter) {
                    acu = acu + this.asignList[j].quantity;
                    console.log("Acumulador" + acu);
                }
            }
            console.log("Acumulador" + (acu + this.asignquantity) + "  >= " + this.rawmatterquantity);
            if ((acu + this.asignquantity) > this.rawmatterquantity) {
                console.log("se paso");
                alert("Cantidad Exedida");
                acu = 0;
                return;
            }
            for (var i = 0; i < this.asignList.length; i++) {
                console.log(i);
                if (this.asignList[i].code == categorycode && this.asignList[i].seccionid == seccionid) {
                    this.asignList[i].quantity = this.asignList[i].quantity + this.asignquantity;
                    return;
                }
            }
            this.quantity = this.asignquantity;
            this.asignList.push({ 'code': categorycode, 'rawmatter': rawmatter, 'seccionid': seccionid, 'quantity': this.asignquantity });
            this.quantity = 1;
        }
        console.log(this.asignList);
    };
    OrdersDetailsComponent.prototype.modalAsignOpen = function (code, name, quantity) {
        this.rawmattercode = code;
        this.rawmattername = name;
        this.rawmatterquantity = quantity;
        console.log("AsignOpen" + this.rawmattercode, this.rawmattername, this.rawmatterquantity);
        this.getCategoryInfo(code);
    };
    OrdersDetailsComponent.prototype.modalOpen = function (id, quantity, code, name) {
        console.log(id);
        this.RAW.id = id;
        this.RAW.quantity = quantity;
        this.RAW.code = code;
        this.RAW.name = name;
        console.log(this.RAW);
    };
    OrdersDetailsComponent.prototype.createOrder = function () {
        // this.orders = new Orders(0,supplierid,1,null,deliverdate,null,descriprion,this.details);
        // console.log("this.orders");
        // console.log(this.orders);
        console.log(this.asignList);
        this.onSubmit();
    };
    OrdersDetailsComponent.prototype.editMaterial = function () {
        var _this = this;
        this._orderdetailsService.updateMatter(this.RAW).subscribe(function (response) {
            _this.status = response.status;
            if (response.status = "success") {
                //this.detailsList = response.detail;
                console.log("Response Update");
                console.log(response);
                _this.getOrderdetails(_this.id);
            }
            else {
                alert("ERROR");
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    OrdersDetailsComponent.prototype.getOrderdetails = function (id) {
        var _this = this;
        this._orderdetailsService.getDetail(id).subscribe(function (response) {
            _this.status = response.status;
            if (response.status = "succes") {
                _this.detailsList = response.details;
                _this.orderdata = response.order;
                console.log(_this.detailsList);
                console.log(_this.orderdata);
                _this.ORDER.id = response.order.id;
                _this.ORDER.registerdate = response.order.registerdate.timestamp;
                _this.ORDER.deliverdate = response.order.deliverdate.timestamp;
                _this.ORDER.description = response.order.description;
                _this.ORDER.status = response.order.status;
            }
            else {
                _this.detailsList = response;
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    OrdersDetailsComponent.prototype.getSeccion = function () {
        var _this = this;
        this._orderdetailsService.getSeccion().subscribe(function (response) {
            _this.seccionList = response;
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    OrdersDetailsComponent.prototype.getCategoryInfo = function (code) {
        var _this = this;
        console.log(code);
        this._orderdetailsService.getCategoryInfo(code).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status != "success") {
                //this.status= "error";
                alert(response.msg);
            }
            else {
                _this.categoryList = response.data;
            }
            // this.categoryList = response;
            // console.log(this.categoryList   )
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    OrdersDetailsComponent = __decorate([
        core_1.Component({
            selector: 'department',
            templateUrl: 'app/view/ordersdetails.html',
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [ordersdetails_service_1.OrdersDetailsService]
        }), 
        __metadata('design:paramtypes', [ordersdetails_service_1.OrdersDetailsService, router_1.ActivatedRoute, router_1.Router])
    ], OrdersDetailsComponent);
    return OrdersDetailsComponent;
}());
exports.OrdersDetailsComponent = OrdersDetailsComponent;
//# sourceMappingURL=ordersdetails.component.js.map