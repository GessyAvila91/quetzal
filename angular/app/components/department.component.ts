/**
 * Created by getsemaniavila on 12/19/16.
 */
import {Component,OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";
import {DepartmentService} from '../services/department.service';
import {Department} from "../model/department";

@Component({
    selector: 'department',
    templateUrl: 'app/view/department.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [DepartmentService]
})

export class DepartmentComponent{
    public titulo="Seccion";
    public department:Department;
    public status;
    public errorMessage;
    public departmentList;
    public productList;

    constructor(
        private _departmentService: DepartmentService,
        private _route: ActivatedRoute,
        private _router:Router
    ){}

    ngOnInit(){
        this.department = new Department(0,"","");
        this.getDepartment();
    }

    onSubmit(){
        console.log(this.department );
        this._departmentService.new(this.department).subscribe(
            response => {
                this.status = response.status;
                console.log(response);
                if(this.status == "success"){
                    this.status = "success";
                    alert("Seccion creada");
                    this.getDepartment();

                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }

    getDepartment(){
        this._departmentService.getDepartment().subscribe(
            response =>{
                /*
                 this.status= response.status;
                 console.log(response);
                 if(this.status!= "success"){
                 this.status= "error";
                 }else{
                 this.productList = response.data;
                 }*/
                this.departmentList = response;
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );
    }

}