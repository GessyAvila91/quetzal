/**
 * Created by getsemaniavila on 12/19/16.
 */
import {Component,OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";
import {DepartmentService} from '../services/department.service';
import {Department} from "../model/department";
import {Productondepartament} from "../model/productondepartment";


@Component({
    selector: 'departmentcontent',
    templateUrl: 'app/view/departmentcontent.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [DepartmentService]
})

export class DepartmentcontentComponent{
    public titulo="Seccion";
    public nombre="{{departamento.id}}";

    public pod;

    public status = "";
    public errorMessage;
    public departmentList;
    public infoList;
    public productList;
    public id;
    public productId = 0;

    public cadena;

    constructor(
        private _route: ActivatedRoute,
        private _router:Router,
        private _departmentService: DepartmentService
    ){}



    ngOnInit(){
        //this.department = new Department(100,"","");
        //this.productondepartament = Productondepartament(1,1,1,1);
        //this.getDepartment();
        //this.getInfo();
        this.pod = {
            "productoName":"nombre",
            "productoId": 0,
            "departamentoId": this.id,
            "cantidad": 0
        };

        this._route.params.subscribe(
            params =>{
                this.id = +params["id"];
                this.getInfo(this.id);
                //this.getDepartamento(this.id);
            }
        );
        this.getProduct();
    }

    modalOpen(proid,name){
        this.pod.productoId = proid;
        this.pod.productoName = name;
    }
    addProduct(cantidad){
        this.pod.cantidad = cantidad;
        this.pod.departamentoId= this.id;
        this.onSubmit();
    }
    onSubmit(){
        console.log(this.pod);
        this._departmentService.add(this.pod).subscribe(
            response => {
                this.status = response.status;
                if(this.status == "success"){
                    alert("Producto Agregado");
                    console.log(response);
                    this.getInfo(this.id);
                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }
    getInfo(id){
        this._departmentService.getInfo(id).subscribe(

            response=>{
                this.status = response.status;
                if(this.status == "success"){
                    this.infoList = response.data;
                    console.log("DaTa"+JSON.stringify(response.data));
                }else{
                }
            },
            error=>{
                this.errorMessage = <any>error;
                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición"+this.errorMessage);
                }
            }/*,
            departamentoinf => {
                this.departmentList = departamentoinf.nombre;
                console.log("depName"+this.departmentList);
                if(this.status = "success"){

                }
            }*/
        );
    }
    getProduct(){
        this._departmentService.getProductList().subscribe(
            response =>{
                /*
                 this.status= response.status;
                 console.log(response);
                 if(this.status!= "success"){
                 this.status= "error";
                 }else{
                 this.productList = response.data;
                 }*/
                console.log("ProductList"+response);
                this.productList= response;
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );
    }
    getDepartamento(id){
        this._departmentService.getInfo(id).subscribe(
            response =>{
                /*
                 this.status= response.status;
                 console.log(response);
                 if(this.status!= "success"){
                 this.status= "error";
                 }else{
                 this.productList = response.data;
                 }*/

                this.nombre = response;
                console.log("M2"+this.nombre);
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );
    }

}