/**
 * Created by getsemaniavila on 12/19/16.
 */
import {Component, OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";
import {RawmatterServices} from '../services/rawmatter.service';
import {Rawmatter} from '../model/rawmatter';

@Component({
    selector: 'rawmatter',
    templateUrl: 'app/view/rawmatter.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [RawmatterServices],
})

export class RawmatterComponent{
    public titulo="Materia Prima";
    public rawmatter:Rawmatter;
    public errorMessage;
    public status;
    public searchCode;

    public rawmatterList;

    constructor(
        private _rawmatterService: RawmatterServices,
        private _route: ActivatedRoute,
        private _router:Router
    ){}

    ngOnInit(){
        this.rawmatter = new Rawmatter(null,null,null,null,null,null);
        this.getRawmatter();
    }
    reset(){
        this.rawmatter.code = null;
        this.rawmatter.name = null;
        this.rawmatter.description = null;

        this.rawmatter.price = null;
        this.rawmatter.minReorder= null;
        this.rawmatter.maxStock = null;
    }
    modalEdit(code,name,description,price,minreorder,maxstock){
        this.rawmatter.code = code;
        this.rawmatter.name = name;
        this.rawmatter.description = description;

        this.rawmatter.price = price;
        this.rawmatter.minReorder= minreorder;
        this.rawmatter.maxStock = maxstock;
    }
    onSubmit(){
        console.log(this.rawmatter);
        this._rawmatterService.new(this.rawmatter).subscribe(
            response => {
                this.status = response.status;
                console.log(response);
                if(this.status != "success"){
                    this.status = "error";
                }else{
                    this.reset();
                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }

        );
    }
    editRawmatter(){
        console.log(this.rawmatter);
        this._rawmatterService.updateRawmatter(this.rawmatter).subscribe(
            response =>{
                 this.status = response.status;
                 if(this.status == "success"){
                     this.getRawmatter();
                 }else{

                 }
            },
            error=>{
                this.errorMessage = <any>error;
                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }

    getRawmatter(){
        this._rawmatterService.getRawmatter().subscribe(
            response =>{
                this.rawmatterList = response;
            },
            error=>{
                this.errorMessage = <any>error;
                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }
    searchRawmatter(){
        this._rawmatterService.searchRawmatter(this.rawmatter).subscribe(
            response =>{
                this.status = response.status;
                if(this.status == "success"){
                    console.log("successs");
                    this.rawmatterList = response.data;
                    console.log(this.rawmatter);
                }else{
                    alert("Error en la petición");
                }
            },
            error=>{
                this.errorMessage = <any>error;
                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }


}