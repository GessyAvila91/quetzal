"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by getsemaniavila on 12/27/16.
 */
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var client_service_1 = require('../services/client.service');
var client_1 = require("../model/client");
var ClientComponent = (function () {
    function ClientComponent(_clientService, _route, _router) {
        this._clientService = _clientService;
        this._route = _route;
        this._router = _router;
        this.titulo = "Agenda de Clientes";
    }
    ClientComponent.prototype.ngOnInit = function () {
        this.client = new client_1.Client(0, "", "", "", "", "", "", "", "", "", "", "", "", "");
        this.getClient();
    };
    ClientComponent.prototype.reset = function () {
        this.client.id = null;
        this.client.name = null;
        this.client.title = null;
        this.client.lastnamepaternal = null;
        this.client.lastnamematernal = null;
        this.client.category = null;
        this.client.rfc = null;
        this.client.company = null;
        this.client.billadress = null;
        this.client.colony = null;
        this.client.state = null;
        this.client.city = null;
        this.client.pc = null;
        this.client.commentary = null;
    };
    ClientComponent.prototype.modalEdit = function (name, title, lastnamepaternal, lastnamematernal, category, rfc, company, billadress, colony, state, city, pc, commentary) {
        this.client.title = title;
        this.client.name = name;
        this.client.lastnamepaternal = lastnamepaternal;
        this.client.lastnamematernal = lastnamematernal;
        this.client.category = category;
        this.client.rfc = rfc;
        this.client.company = company;
        this.client.billadress = billadress;
        this.client.colony = colony;
        this.client.state = state;
        this.client.city = city;
        this.client.pc = pc;
        this.client.commentary = commentary;
    };
    ClientComponent.prototype.onSubmit = function () {
        var _this = this;
        console.log(this.client);
        this._clientService.new(this.client).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status != "success") {
                _this.status = "error";
            }
            else {
                _this.reset();
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    ClientComponent.prototype.editClient = function () {
    };
    ClientComponent.prototype.getClient = function () {
        var _this = this;
        this._clientService.getClient().subscribe(function (response) {
            /*
             this.status= response.status;
             console.log(response);
             if(this.status!= "success"){
             this.status= "error";
             }else{
             this.productList = response.data;
             }*/
            _this.clientList = response;
            console.log(_this.clientList);
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    ClientComponent = __decorate([
        core_1.Component({
            selector: 'client',
            templateUrl: 'app/view/client.html',
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [client_service_1.ClientService]
        }), 
        __metadata('design:paramtypes', [client_service_1.ClientService, router_1.ActivatedRoute, router_1.Router])
    ], ClientComponent);
    return ClientComponent;
}());
exports.ClientComponent = ClientComponent;
//# sourceMappingURL=client.component.js.map