/**
 * Created by GessyAvila on 26-Jul-17.
 */
import {Component, OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES,Router,ActivatedRoute} from "@angular/router";
import {CategoryService} from '../services/category.service';
import {Category} from '../model/category';

@Component({
    selector: 'category',
    templateUrl: 'app/view/category.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [CategoryService]
})

export class CategoryComponent{
    public titulo="Category : de <<rawmmatter>>";
    public category:Category
    public errorMessage;
    public status;
    public code = "";

    public categoryList;

    constructor(
        private _categoryService: CategoryService,
        private _route: ActivatedRoute,
        private _router:Router
    ){}

    ngOnInit(){
        this.category = new Category(null,null,null,null);

        this._route.params.subscribe(
            params =>{
                this.code = params["code"] ;
                this.category = new Category(null,this.code,null,null);
                console.log(this.code)
                this.getInfo(this.code);
                console.log(params);
            }

        );

    }
    sussess(){
        this.reset();
        this.getInfo(this.code);
    }
    reset(){
        this.category.code = null;
        this.category.name = null;
        this.category.rawmatterCode = null;
        this.category.description = null;
    }
    modalEdit(code,name,rawmatterCode,description,price,minreorder,maxstock){
        this.category.code = code;
        this.category.name = name;
        this.category.rawmatterCode = rawmatterCode
        this.category.description = description;
    }
    onSubmit(){
        console.log(this.category);
        this._categoryService.new(this.category).subscribe(
            response => {
                this.status = response.status;
                console.log(response);
                if(this.status != "success"){
                    this.status = "error";
                }else{
                    this.sussess();
                    alert(response.msg)
                }
            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    alert("Error en la petición");
                }
            }
        );
    }
    editcategory(){

    }
    getInfo(code){
        console.log(code);
        this._categoryService.getInfo(code).subscribe(
            response =>{
                 this.status= response.status;
                 console.log(response);
                 if(this.status!= "success"){
                    //this.status= "error";
                     alert("Materia Prima no cuenta con categorías, favor de agregarlas \nServer Response"+response.msg)
                     this.titulo = "Categoria de: " + response.rawmatter.code + " / " + response.rawmatter.name;
                 }else{
                     this.titulo = "Categoria de: " + response.rawmatter.code + " / " + response.rawmatter.name;
                     this.categoryList  = response.data;

                 }
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );
    }
    getCategory(){
        this._categoryService.getCategory().subscribe(
            response =>{
                /*
                 this.status= response.status;
                 console.log(response);
                 if(this.status!= "success"){
                 this.status= "error";
                 }else{
                 this.productList = response.data;
                 }*/
                this.categoryList = response;
                console.log(this.categoryList)
            },
            error=>{
                this.errorMessage = <any>error;

                if(this.errorMessage != null){
                    console.log(this.errorMessage);
                    //alert("Error en la petición");
                }
            }
        );
    }
}