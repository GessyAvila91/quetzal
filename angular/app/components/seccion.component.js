"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by GessyAvila on 28-Jul-17.
 */
/**
 * Created by getsemaniavila on 12/19/16.
 */
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var seccion_service_1 = require('../services/seccion.service');
var seccion_1 = require("../model/seccion");
var SeccionComponent = (function () {
    function SeccionComponent(_seccionService, _route, _router) {
        this._seccionService = _seccionService;
        this._route = _route;
        this._router = _router;
        this.titulo = "Seccion";
    }
    SeccionComponent.prototype.ngOnInit = function () {
        this.seccion = new seccion_1.Seccion(null, null, null);
        this.getSeccion();
    };
    SeccionComponent.prototype.reset = function () {
        this.seccion = new seccion_1.Seccion(null, null, null);
    };
    SeccionComponent.prototype.onSubmit = function () {
        var _this = this;
        console.log(this.seccion);
        this._seccionService.new(this.seccion).subscribe(function (response) {
            _this.status = response.status;
            console.log(response);
            if (_this.status == "success") {
                _this.status = "success";
                alert(response.msg);
                _this.getSeccion();
                _this.reset();
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
                alert("Error en la petición");
            }
        });
    };
    SeccionComponent.prototype.getSeccion = function () {
        var _this = this;
        this._seccionService.getSeccion().subscribe(function (response) {
            /*
             this.status= response.status;
             console.log(response);
             if(this.status!= "success"){
             this.status= "error";
             }else{
             this.productList = response.data;
             }*/
            _this.seccionList = response;
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage != null) {
                console.log(_this.errorMessage);
            }
        });
    };
    SeccionComponent = __decorate([
        core_1.Component({
            selector: 'seccion',
            templateUrl: 'app/view/seccion.html',
            directives: [router_1.ROUTER_DIRECTIVES],
            providers: [seccion_service_1.SeccionService]
        }), 
        __metadata('design:paramtypes', [seccion_service_1.SeccionService, router_1.ActivatedRoute, router_1.Router])
    ], SeccionComponent);
    return SeccionComponent;
}());
exports.SeccionComponent = SeccionComponent;
//# sourceMappingURL=seccion.component.js.map