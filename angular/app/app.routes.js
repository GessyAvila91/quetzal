"use strict";
var router_1 = require("@angular/router");
var default_component_1 = require("./components/default.component");
var login_component_1 = require("./components/login.component");
var department_component_1 = require("./components/department.component");
var departmentcontent_component_1 = require("./components/departmentcontent.component");
var product_component_1 = require("./components/product.component");
var merch_component_1 = require("./components/merch.component");
var supplier_component_1 = require("./components/supplier.component");
var sale_component_1 = require("./components/sale.component");
var client_component_1 = require("./components/client.component");
var clientinfo_component_1 = require("./components/clientinfo.component");
var orders_component_1 = require("./components/orders.component");
var ordersdetails_component_1 = require("./components/ordersdetails.component");
var rawmatter_component_1 = require("./components/rawmatter.component");
var category_component_1 = require("./components/category.component");
var seccion_component_1 = require("./components/seccion.component");
var seccioncontent_component_1 = require("./components/seccioncontent.component");
exports.routes = [
    {
        path: '',
        redirectTo: '/index',
        terminal: true
    },
    { path: 'index', component: default_component_1.DefaultComponent },
    { path: 'department', component: department_component_1.DepartmentComponent },
    { path: 'departmentcontent', component: departmentcontent_component_1.DepartmentcontentComponent },
    { path: 'departmentcontent/:id', component: departmentcontent_component_1.DepartmentcontentComponent },
    { path: 'product', component: product_component_1.ProductComponent },
    { path: 'merch', component: merch_component_1.MerchComponent },
    { path: 'supplier', component: supplier_component_1.SupplierComponent },
    { path: 'orders', component: orders_component_1.OrdersComponent },
    { path: 'ordersdetails', component: ordersdetails_component_1.OrdersDetailsComponent },
    { path: 'ventas', component: clientinfo_component_1.ClientInfoComponent },
    { path: 'login/:id', component: login_component_1.LoginComponent },
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'sale', component: sale_component_1.SaleComponent },
    { path: 'rawmatter', component: rawmatter_component_1.RawmatterComponent },
    { path: 'category', component: category_component_1.CategoryComponent },
    { path: 'category/:code', component: category_component_1.CategoryComponent },
    { path: 'client', component: client_component_1.ClientComponent },
    { path: 'client/info/:id', component: clientinfo_component_1.ClientInfoComponent },
    { path: 'seccion', component: seccion_component_1.SeccionComponent },
    { path: 'seccioncontent/:id', component: seccioncontent_component_1.SeccioncontentComponent },
    { path: 'ordersdetails/:id', component: ordersdetails_component_1.OrdersDetailsComponent }
];
exports.APP_ROUTER_PROVIDERS = [
    router_1.provideRouter(exports.routes)
];
//# sourceMappingURL=app.routes.js.map