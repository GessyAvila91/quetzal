import {provideRouter, RouterConfig} from "@angular/router";

import {DefaultComponent} from "./components/default.component";
import {RegisterComponent} from "./components/register.component";
import {LoginComponent} from "./components/login.component";
import {DepartmentComponent} from "./components/department.component";
import {DepartmentcontentComponent} from "./components/departmentcontent.component";
import {ProductComponent} from "./components/product.component";
import {MerchComponent} from "./components/merch.component";
import {SupplierComponent} from "./components/supplier.component";
import {SaleComponent} from "./components/sale.component";
import {ClientComponent} from "./components/client.component";
import {ClientInfoComponent} from "./components/clientinfo.component";
import {OrdersComponent} from "./components/orders.component";
import {OrdersDetailsComponent} from "./components/ordersdetails.component";



import {RawmatterComponent} from "./components/rawmatter.component";
import {CategoryComponent} from "./components/category.component";
import {SeccionComponent} from "./components/seccion.component";
import {SeccioncontentComponent} from "./components/seccioncontent.component";


export const routes: RouterConfig = [
    {
        path:'',
        redirectTo:'/index',
        terminal:true
    },

    {path: 'index',component: DefaultComponent},
    {path: 'department',component: DepartmentComponent},
    {path: 'departmentcontent',component: DepartmentcontentComponent},
    {path: 'departmentcontent/:id',component: DepartmentcontentComponent},
    {path: 'product',component: ProductComponent},
    {path: 'merch',component: MerchComponent},
    {path: 'supplier',component: SupplierComponent},
    {path: 'orders',component: OrdersComponent},
    {path: 'ordersdetails',component: OrdersDetailsComponent},
    {path: 'ventas',component: ClientInfoComponent},
    {path: 'login/:id',component: LoginComponent},
    {path: 'login',component: LoginComponent},
    {path: 'sale',component: SaleComponent},


    {path: 'rawmatter',component: RawmatterComponent},
    {path: 'category',component: CategoryComponent},
    {path: 'category/:code',component: CategoryComponent},

    {path: 'client',component: ClientComponent},
    {path: 'client/info/:id',component: ClientInfoComponent},
    {path: 'seccion',component: SeccionComponent},
    {path: 'seccioncontent/:id',component: SeccioncontentComponent},

    {path: 'ordersdetails/:id',component: OrdersDetailsComponent}


];


export const APP_ROUTER_PROVIDERS = [
    provideRouter(routes)
];