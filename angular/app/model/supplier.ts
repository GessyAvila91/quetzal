/**
 * Created by getsemaniavila on 12/20/16.
 */

export class Supplier{
    constructor(
        public id:number,
        public tradename:string,
        public businessName:string,
        public contactPerson:number,
        public addres:string,

        public phone:string,
        public email:string,
        public webpage:string,
        public rfc:string,
        public description:string
    ){}
}