/**
 * Created by GessyAvila on 27-Jul-17.
 */
export class Card{
    constructor(
        public id:number,
        public clientid:number,
        public number:string,
        public nombre:string
    ){}
}