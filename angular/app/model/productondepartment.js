"use strict";
var Productondepartament = (function () {
    function Productondepartament(ids, productid, departmentid, quantity) {
        this.ids = ids;
        this.productid = productid;
        this.departmentid = departmentid;
        this.quantity = quantity;
    }
    return Productondepartament;
}());
exports.Productondepartament = Productondepartament;
//# sourceMappingURL=productondepartment.js.map