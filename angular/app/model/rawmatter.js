"use strict";
/**
 * Created by getsemaniavila on 12/20/16.
 */
var Rawmatter = (function () {
    function Rawmatter(code, name, description, price, minReorder, maxStock) {
        this.code = code;
        this.name = name;
        this.description = description;
        this.price = price;
        this.minReorder = minReorder;
        this.maxStock = maxStock;
    }
    return Rawmatter;
}());
exports.Rawmatter = Rawmatter;
//# sourceMappingURL=rawmatter.js.map