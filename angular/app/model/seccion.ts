/**
 * Created by GessyAvila on 28-Jul-17.
 */
/**
 * Created by getsemaniavila on 12/20/16.
 */
export class Seccion{
    constructor(
        public id:number,
        public name:string,
        public description:string
    ){}
}