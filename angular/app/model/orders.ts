/**
 * Created by GessyAvila on 28-Jul-17.
 */
export class Orders{
    constructor(
        public id:number,
        public supplierid:number,
        public userid:number,
        public registerdate:string,
        public deliverdate:string,
        public receptiondate:string,
        public description:string,
        public details:Ordersdetail[]
    ){}
}

export class Ordersdetail{
    constructor(
        public rawmattercode:string,
        public quantity:number,
        public price
    ){}
}
