/**
 * Created by Gessy on 18/01/2017.
 */
export class Client{
    constructor(
        public id:number,
        public title:string,
        public name:string,
        public lastnamepaternal:string,
        public lastnamematernal:string,

        public category:string,
        public rfc:string,
        public company:string,
        public billadress:string,
        public colony:string,

        public state:string,
        public city:string,
        public pc:string,
        public commentary:string
    ){}
}