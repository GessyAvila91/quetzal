"use strict";
/**
 * Created by GessyAvila on 26-Jul-17.
 */
var Category = (function () {
    function Category(code, rawmatterCode, name, description) {
        this.code = code;
        this.rawmatterCode = rawmatterCode;
        this.name = name;
        this.description = description;
    }
    return Category;
}());
exports.Category = Category;
//# sourceMappingURL=category.js.map