"use strict";
/**
 * Created by getsemaniavila on 12/20/16.
 */
var Product = (function () {
    function Product(id, codigo, name, descripcion, stock, reordenMin, maximoStock) {
        this.id = id;
        this.codigo = codigo;
        this.name = name;
        this.descripcion = descripcion;
        this.stock = stock;
        this.reordenMin = reordenMin;
        this.maximoStock = maximoStock;
    }
    return Product;
}());
exports.Product = Product;
//# sourceMappingURL=product.js.map