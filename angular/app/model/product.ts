/**
 * Created by getsemaniavila on 12/20/16.
 */
export class Product{
    constructor(
        public id:number,
        public codigo:number,
        public name:string,
        public descripcion:string,
        public stock:number,
        public reordenMin:number,
        public maximoStock:number

    ){}
}
