/**
 * Created by GessyAvila on 26-Jul-17.
 */
export class Category{
    constructor(
        public code:string,
        public rawmatterCode:string,
        public name:string,
        public description:string
    ){}
}
