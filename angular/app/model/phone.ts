/**
 * Created by GessyAvila on 26-Jul-17.
 */
export class Phone{
    constructor(
        public id:number,
        public clientid:number,
        public number:string,
        public description:string
    ){}
}