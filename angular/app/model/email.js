/**
 * Created by GessyAvila on 26-Jul-17.
 */
"use strict";
var Email = (function () {
    function Email(id, clientid, email, description) {
        this.id = id;
        this.clientid = clientid;
        this.email = email;
        this.description = description;
    }
    return Email;
}());
exports.Email = Email;
//# sourceMappingURL=email.js.map