"use strict";
/**
 * Created by GessyAvila on 28-Jul-17.
 */
var Orders = (function () {
    function Orders(id, supplierid, userid, registerdate, deliverdate, receptiondate, description, details) {
        this.id = id;
        this.supplierid = supplierid;
        this.userid = userid;
        this.registerdate = registerdate;
        this.deliverdate = deliverdate;
        this.receptiondate = receptiondate;
        this.description = description;
        this.details = details;
    }
    return Orders;
}());
exports.Orders = Orders;
var Ordersdetail = (function () {
    function Ordersdetail(rawmattercode, quantity, price) {
        this.rawmattercode = rawmattercode;
        this.quantity = quantity;
        this.price = price;
    }
    return Ordersdetail;
}());
exports.Ordersdetail = Ordersdetail;
//# sourceMappingURL=orders.js.map