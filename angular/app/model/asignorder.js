"use strict";
/**
 * Created by GessyAvila on 28-Jul-17.
 */
var Asignorder = (function () {
    function Asignorder(id, details) {
        this.id = id;
        this.details = details;
    }
    return Asignorder;
}());
exports.Asignorder = Asignorder;
var Ordersdetail = (function () {
    function Ordersdetail(code, rawmatter, seccionid, quantity) {
        this.code = code;
        this.rawmatter = rawmatter;
        this.seccionid = seccionid;
        this.quantity = quantity;
    }
    return Ordersdetail;
}());
exports.Ordersdetail = Ordersdetail;
//# sourceMappingURL=asignorder.js.map