"use strict";
/**
 * Created by Gessy on 18/01/2017.
 */
var Client = (function () {
    function Client(id, title, name, lastnamepaternal, lastnamematernal, category, rfc, company, billadress, colony, state, city, pc, commentary) {
        this.id = id;
        this.title = title;
        this.name = name;
        this.lastnamepaternal = lastnamepaternal;
        this.lastnamematernal = lastnamematernal;
        this.category = category;
        this.rfc = rfc;
        this.company = company;
        this.billadress = billadress;
        this.colony = colony;
        this.state = state;
        this.city = city;
        this.pc = pc;
        this.commentary = commentary;
    }
    return Client;
}());
exports.Client = Client;
//# sourceMappingURL=client.js.map