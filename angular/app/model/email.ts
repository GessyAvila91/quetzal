/**
 * Created by GessyAvila on 26-Jul-17.
 */

export class Email{
    constructor(
        public id:number,
        public clientid:number,
        public email:string,
        public description:string
    ){}
}
