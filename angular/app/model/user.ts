/**
 * Created by getsemaniavila on 12/19/16.
 */
export class User{
    constructor(
        public id:number,
        public role:string,
        public name:string,
        public surname:string,
        public email:string,
        public password:string,
        public image:string
    ){}
}