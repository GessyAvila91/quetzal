"use strict";
/**
 * Created by GessyAvila on 28-Jul-17.
 */
/**
 * Created by getsemaniavila on 12/20/16.
 */
var Seccion = (function () {
    function Seccion(id, name, description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }
    return Seccion;
}());
exports.Seccion = Seccion;
//# sourceMappingURL=seccion.js.map