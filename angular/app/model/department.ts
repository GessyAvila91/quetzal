/**
 * Created by getsemaniavila on 12/20/16.
 */
export class Department{
    constructor(
        public id:number,
        public nombre:string,
        public descripcion:string
    ){}
}