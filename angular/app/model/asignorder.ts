/**
 * Created by GessyAvila on 28-Jul-17.
 */
export class Asignorder{
    constructor(
        public id:number,
        public details:Ordersdetail[]
    ){}
}

export class Ordersdetail{
    constructor(
        public code:string,
        public rawmatter:number,
        public seccionid:number,
        public quantity:number
    ){}
}
