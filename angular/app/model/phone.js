"use strict";
/**
 * Created by GessyAvila on 26-Jul-17.
 */
var Phone = (function () {
    function Phone(id, clientid, number, description) {
        this.id = id;
        this.clientid = clientid;
        this.number = number;
        this.description = description;
    }
    return Phone;
}());
exports.Phone = Phone;
//# sourceMappingURL=phone.js.map