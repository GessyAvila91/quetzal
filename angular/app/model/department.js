"use strict";
/**
 * Created by getsemaniavila on 12/20/16.
 */
var Department = (function () {
    function Department(id, nombre, descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }
    return Department;
}());
exports.Department = Department;
//# sourceMappingURL=department.js.map