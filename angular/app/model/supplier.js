/**
 * Created by getsemaniavila on 12/20/16.
 */
"use strict";
var Supplier = (function () {
    function Supplier(id, tradename, businessName, contactPerson, addres, phone, email, webpage, rfc, description) {
        this.id = id;
        this.tradename = tradename;
        this.businessName = businessName;
        this.contactPerson = contactPerson;
        this.addres = addres;
        this.phone = phone;
        this.email = email;
        this.webpage = webpage;
        this.rfc = rfc;
        this.description = description;
    }
    return Supplier;
}());
exports.Supplier = Supplier;
//# sourceMappingURL=supplier.js.map