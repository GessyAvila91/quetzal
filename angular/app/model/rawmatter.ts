/**
 * Created by getsemaniavila on 12/20/16.
 */
export class Rawmatter{
    constructor(
        public code:string,
        public name:string,
        public description:string,
        public price:number,
        public minReorder:number,
        public maxStock:number
    ){}
}
