"use strict";
/**
 * Created by GessyAvila on 27-Jul-17.
 */
var Card = (function () {
    function Card(id, clientid, number, nombre) {
        this.id = id;
        this.clientid = clientid;
        this.number = number;
        this.nombre = nombre;
    }
    return Card;
}());
exports.Card = Card;
//# sourceMappingURL=card.js.map