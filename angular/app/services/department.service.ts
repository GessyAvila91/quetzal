import {Injectable} from "@angular/core";
import {Http,Response,Headers} from "@angular/http";
import {Observable} from "rxjs/observable";
import "rxjs/add/operator/map";
import _UrlServices = require('./url.service');


@Injectable()
export class DepartmentService{
    public url = _UrlServices.weburl;
    constructor(private _http:Http){}


    new(department_to_register){
        let json = JSON.stringify(department_to_register);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/seccion/new", params, {headers: headers}).map(res => res.json());
    }
    add(department_to_register){
        let json = JSON.stringify(department_to_register);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/productoondepartamento/new", params, {headers: headers}).map(res => res.json());
    }
    getProduct(id){
        return this._http.get(this.url+"/productoondepartamento/asociacion/"+id).map(res => res.json());
    }
    getProductList(){
        return this._http.get(this.url+"/producto").map(res => res.json());
    }
    getInfo(id){
        return this._http.get(this.url+"/productoondepartamento/info/"+id).map(res => res.json());
    }
    getDepartment(){
        return this._http.get(this.url+"/seccion").map(res => res.json());
    }
}
