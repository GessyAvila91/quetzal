/**
 * Created by getsemaniavila on 12/19/16.
 */
import {Injectable} from "@angular/core";
import {Http,Response,Headers} from "@angular/http";
import {Observable} from "rxjs/observable";
import "rxjs/add/operator/map";
import _UrlServices = require('./url.service');
@Injectable()

export class LoginService{

    public url = _UrlServices.weburl;
    public identity;
    public token;

    constructor(private _http:Http){}

    signup(user_to_login){
        let json= JSON.stringify(user_to_login);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/login",params,{headers:headers}).map(res => res.json());
    }

    getIdentity(){
        let identity = JSON.parse(localStorage.getItem('identity'));

        if(identity != "undefined"){
            this.identity = identity;
        }else{
            this.identity = null;
        }
        return this.identity;
    }

    getToken(){
        let token = localStorage.getItem('token');

        if(token != "undefined"){
            this.token = token;
        }else{
            this.token = null;
        }
        return this.token;
    }

    register(user_to_register){
        let json = JSON.stringify(user_to_register);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/user/new", params, {headers: headers}).map(res => res.json());
    }
}
