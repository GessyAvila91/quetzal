"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by GessyAvila on 28-Jul-17.
 */
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var _UrlServices = require('./url.service');
var SeccionService = (function () {
    function SeccionService(_http) {
        this._http = _http;
        this.url = _UrlServices.weburl;
    }
    SeccionService.prototype.new = function (seccion_to_register) {
        var json = JSON.stringify(seccion_to_register);
        var params = "json=" + json;
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this._http.post(this.url + "/seccion/new", params, { headers: headers }).map(function (res) { return res.json(); });
    };
    SeccionService.prototype.add = function (matter_to_register) {
        var json = JSON.stringify(matter_to_register);
        var params = "json=" + json;
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this._http.post(this.url + "/seccion/add", params, { headers: headers }).map(function (res) { return res.json(); });
    };
    SeccionService.prototype.edit = function (matter_to_edit) {
        var json = JSON.stringify(matter_to_edit);
        var params = "json=" + json;
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this._http.post(this.url + "/seccion/editmatter", params, { headers: headers }).map(function (res) { return res.json(); });
    };
    SeccionService.prototype.getSeccion = function () {
        return this._http.get(this.url + "/seccion").map(function (res) { return res.json(); });
    };
    SeccionService.prototype.getCategoryList = function () {
        return this._http.get(this.url + "/category").map(function (res) { return res.json(); });
    };
    SeccionService.prototype.getInfo = function (id) {
        return this._http.get(this.url + "/seccion/content/" + id).map(function (res) { return res.json(); });
    };
    SeccionService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], SeccionService);
    return SeccionService;
}());
exports.SeccionService = SeccionService;
//# sourceMappingURL=seccion.service.js.map