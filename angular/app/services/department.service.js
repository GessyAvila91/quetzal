"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var _UrlServices = require('./url.service');
var DepartmentService = (function () {
    function DepartmentService(_http) {
        this._http = _http;
        this.url = _UrlServices.weburl;
    }
    DepartmentService.prototype.new = function (department_to_register) {
        var json = JSON.stringify(department_to_register);
        var params = "json=" + json;
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this._http.post(this.url + "/seccion/new", params, { headers: headers }).map(function (res) { return res.json(); });
    };
    DepartmentService.prototype.add = function (department_to_register) {
        var json = JSON.stringify(department_to_register);
        var params = "json=" + json;
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this._http.post(this.url + "/productoondepartamento/new", params, { headers: headers }).map(function (res) { return res.json(); });
    };
    DepartmentService.prototype.getProduct = function (id) {
        return this._http.get(this.url + "/productoondepartamento/asociacion/" + id).map(function (res) { return res.json(); });
    };
    DepartmentService.prototype.getProductList = function () {
        return this._http.get(this.url + "/producto").map(function (res) { return res.json(); });
    };
    DepartmentService.prototype.getInfo = function (id) {
        return this._http.get(this.url + "/productoondepartamento/info/" + id).map(function (res) { return res.json(); });
    };
    DepartmentService.prototype.getDepartment = function () {
        return this._http.get(this.url + "/seccion").map(function (res) { return res.json(); });
    };
    DepartmentService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], DepartmentService);
    return DepartmentService;
}());
exports.DepartmentService = DepartmentService;
//# sourceMappingURL=department.service.js.map