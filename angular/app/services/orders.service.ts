import {Injectable} from "@angular/core";
import {Http,Response,Headers} from "@angular/http";
import {Observable} from "rxjs/observable";
import "rxjs/add/operator/map";
import _UrlServices = require('./url.service');

@Injectable()
export class OrdersService{

    public url = _UrlServices.weburl;
    constructor(private _http:Http){}

    new(orders_to_register){
        let json = JSON.stringify(orders_to_register);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/order/new", params, {headers: headers}).map(res => res.json());
    }
    getOrder(){
        return this._http.get(this.url+"/order").map(res => res.json());
    }
    getAll(){
        return this._http.get(this.url+"/order/getall").map(res => res.json());
    }

    getSupplier(){
        return this._http.get(this.url+"/supplier").map(res => res.json());
    }
    getRawmatter(){
        return this._http.get(this.url+"/rawmatter").map(res => res.json());
    }


}



