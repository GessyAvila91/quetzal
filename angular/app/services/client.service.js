"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by getsemaniavila on 12/27/16.
 */
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var _UrlServices = require('./url.service');
var ClientService = (function () {
    function ClientService(_http) {
        this._http = _http;
        this.url = _UrlServices.weburl;
    }
    ClientService.prototype.new = function (client_to_register) {
        var json = JSON.stringify(client_to_register);
        var params = "json=" + json;
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this._http.post(this.url + "/client/new", params, { headers: headers }).map(function (res) { return res.json(); });
    };
    ClientService.prototype.edit = function (client_to_edit) {
        var json = JSON.stringify(client_to_edit);
        var params = "json=" + json;
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this._http.post(this.url + "/client/edit", params, { headers: headers }).map(function (res) { return res.json(); });
    };
    ClientService.prototype.newphone = function (phone_to_register, id) {
        var json = JSON.stringify(phone_to_register);
        var params = "json=" + json;
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this._http.post(this.url + "/client/phone/" + id, params, { headers: headers }).map(function (res) { return res.json(); });
    };
    ClientService.prototype.newemail = function (email_to_register, id) {
        var json = JSON.stringify(email_to_register);
        var params = "json=" + json;
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this._http.post(this.url + "/client/email/" + id, params, { headers: headers }).map(function (res) { return res.json(); });
    };
    ClientService.prototype.emailedit = function (email_to_edit) {
        var json = JSON.stringify(email_to_edit);
        var params = "json=" + json;
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this._http.post(this.url + "/client/emailedit", params, { headers: headers }).map(function (res) { return res.json(); });
    };
    ClientService.prototype.phoneedit = function (phone_to_edit) {
        var json = JSON.stringify(phone_to_edit);
        var params = "json=" + json;
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this._http.post(this.url + "/client/phoneedit", params, { headers: headers }).map(function (res) { return res.json(); });
    };
    ClientService.prototype.getClient = function () {
        return this._http.get(this.url + "/client").map(function (res) { return res.json(); });
    };
    ClientService.prototype.getInfo = function (id) {
        return this._http.get(this.url + "/client/info/" + id).map(function (res) { return res.json(); });
    };
    ClientService.prototype.getPurchar = function () {
        return this._http.get(this.url + "/client/compras").map(function (res) { return res.json(); });
    };
    ClientService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], ClientService);
    return ClientService;
}());
exports.ClientService = ClientService;
//# sourceMappingURL=client.service.js.map