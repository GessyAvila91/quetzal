import {Injectable} from "@angular/core";
import {Http,Response,Headers} from "@angular/http";
import {Observable} from "rxjs/observable";
import "rxjs/add/operator/map";
import _UrlServices = require('./url.service');

@Injectable()
export class OrdersDetailsService{

    public url = _UrlServices.weburl;
    constructor(private _http:Http){}

    new(product_to_register){
        let json = JSON.stringify(product_to_register);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/ordenes/new", params, {headers: headers}).map(res => res.json());
    }
    getDetail(id){
        return this._http.get(this.url+"/order/detail/"+id).map(res => res.json());
    }
    getProduct(){
        return this._http.get(this.url+"/ordenes").map(res => res.json());
    }

    updateMatter(matter_to_update){
        let json = JSON.stringify(matter_to_update);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/order/update", params, {headers: headers}).map(res => res.json());
    }
    getCategoryInfo(code){
        return this._http.get(this.url+"/category/info/"+code).map(res => res.json());
    }
    getSeccion(){
        return this._http.get(this.url+"/seccion").map(res => res.json());
    }
    asignComplete(order_to_asign){
        let json = JSON.stringify(order_to_asign);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/order/complete", params, {headers: headers}).map(res => res.json());
    }
}

