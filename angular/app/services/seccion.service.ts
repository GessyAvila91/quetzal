/**
 * Created by GessyAvila on 28-Jul-17.
 */
import {Injectable} from "@angular/core";
import {Http,Response,Headers} from "@angular/http";
import {Observable} from "rxjs/observable";
import "rxjs/add/operator/map";
import _UrlServices = require('./url.service');

@Injectable()
export class SeccionService{
    public url = _UrlServices.weburl;
    constructor(private _http:Http){}


    new(seccion_to_register){
        let json = JSON.stringify(seccion_to_register);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/seccion/new", params, {headers: headers}).map(res => res.json());
    }
    add(matter_to_register){
        let json = JSON.stringify(matter_to_register);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/seccion/add", params, {headers: headers}).map(res => res.json());
    }
    edit(matter_to_edit){
        let json = JSON.stringify(matter_to_edit);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/seccion/editmatter", params, {headers: headers}).map(res => res.json());
    }
    getSeccion(){
        return this._http.get(this.url+"/seccion").map(res => res.json());
    }
    getCategoryList(){
        return this._http.get(this.url+"/category").map(res => res.json());
    }
    getInfo(id){
        return this._http.get(this.url+"/seccion/content/"+id).map(res => res.json());
    }
}
