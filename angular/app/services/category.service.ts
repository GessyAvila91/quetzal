/**
 * Created by GessyAvila on 26-Jul-17.
 */
import {Injectable} from "@angular/core";
import {Http,Response,Headers} from "@angular/http";
import {Observable} from "rxjs/observable";
import "rxjs/add/operator/map";
import _UrlServices = require('./url.service');

@Injectable()
export class CategoryService{

    public url = _UrlServices.weburl;
    constructor(private _http:Http){}

    new(category_to_register){
        let json = JSON.stringify(category_to_register);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/category/new", params, {headers: headers}).map(res => res.json());
    }
    getCategory(){
        return this._http.get(this.url+"/category").map(res => res.json());
    }
    getInfo(code){
        return this._http.get(this.url+"/category/info/"+code).map(res => res.json());
    }
}
