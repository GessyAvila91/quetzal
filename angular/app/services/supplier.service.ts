/**
 * Created by getsemaniavila on 12/20/16.
 */
import {Injectable} from "@angular/core";
import {Http,Response,Headers} from "@angular/http";
import {Observable} from "rxjs/observable";
import "rxjs/add/operator/map";
import _UrlServices = require('./url.service');


@Injectable()
export class SupplierService{
    public url = _UrlServices.weburl;

    constructor(private _http:Http){}

    new(supplier_to_register){
        let json = JSON.stringify(supplier_to_register);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/supplier/new", params, {headers: headers}).map(res => res.json());
    }
    edit(seccion_to_edit){
        let json = JSON.stringify(seccion_to_edit);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/supplier/edit", params, {headers: headers}).map(res => res.json());
    }
    getSupplier(){
        return this._http.get(this.url+"/supplier").map(res => res.json());
    }
}
