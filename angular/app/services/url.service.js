"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
require("rxjs/add/operator/map");
'use strict';
// export var weburl  = "http://api.floreriadelvalle.net/symfony/web/app_dev.php";
// sexport var weburl  = "http://192.168.0.100/www/Quetzal/apiQuetzal/web/app_dev.php";
// export var weburl    = "http://localhost/www/Quetzal/apiQuetzal/web/app_dev.php";
exports.weburl = "http://api.floreriadelvalle.net/apiQuetzal/web/app_dev.php";
var UrlService = (function () {
    function UrlService() {
    }
    UrlService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], UrlService);
    return UrlService;
}());
exports.UrlService = UrlService;
//# sourceMappingURL=url.service.js.map