"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var _UrlServices = require('./url.service');
var OrdersDetailsService = (function () {
    function OrdersDetailsService(_http) {
        this._http = _http;
        this.url = _UrlServices.weburl;
    }
    OrdersDetailsService.prototype.new = function (product_to_register) {
        var json = JSON.stringify(product_to_register);
        var params = "json=" + json;
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this._http.post(this.url + "/ordenes/new", params, { headers: headers }).map(function (res) { return res.json(); });
    };
    OrdersDetailsService.prototype.getDetail = function (id) {
        return this._http.get(this.url + "/order/detail/" + id).map(function (res) { return res.json(); });
    };
    OrdersDetailsService.prototype.getProduct = function () {
        return this._http.get(this.url + "/ordenes").map(function (res) { return res.json(); });
    };
    OrdersDetailsService.prototype.updateMatter = function (matter_to_update) {
        var json = JSON.stringify(matter_to_update);
        var params = "json=" + json;
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this._http.post(this.url + "/order/update", params, { headers: headers }).map(function (res) { return res.json(); });
    };
    OrdersDetailsService.prototype.getCategoryInfo = function (code) {
        return this._http.get(this.url + "/category/info/" + code).map(function (res) { return res.json(); });
    };
    OrdersDetailsService.prototype.getSeccion = function () {
        return this._http.get(this.url + "/seccion").map(function (res) { return res.json(); });
    };
    OrdersDetailsService.prototype.asignComplete = function (order_to_asign) {
        var json = JSON.stringify(order_to_asign);
        var params = "json=" + json;
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this._http.post(this.url + "/order/complete", params, { headers: headers }).map(function (res) { return res.json(); });
    };
    OrdersDetailsService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], OrdersDetailsService);
    return OrdersDetailsService;
}());
exports.OrdersDetailsService = OrdersDetailsService;
//# sourceMappingURL=ordersdetails.service.js.map