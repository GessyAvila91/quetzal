import {Injectable} from "@angular/core";
import {Http,Response,Headers} from "@angular/http";
import {Observable} from "rxjs/observable";
import "rxjs/add/operator/map";
import _UrlServices = require('./url.service');

@Injectable()
export class ProductService{

    public url = _UrlServices.weburl;
    constructor(private _http:Http){}

    new(product_to_register){
        let json = JSON.stringify(product_to_register);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/rawmatter/new", params, {headers: headers}).map(res => res.json());
    }
    getProduct(){
        return this._http.get(this.url+"/rawmatter/").map(res => res.json());
    }

}