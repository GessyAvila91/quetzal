"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var _UrlServices = require('./url.service');
var OrdersService = (function () {
    function OrdersService(_http) {
        this._http = _http;
        this.url = _UrlServices.weburl;
    }
    OrdersService.prototype.new = function (orders_to_register) {
        var json = JSON.stringify(orders_to_register);
        var params = "json=" + json;
        var headers = new http_1.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        return this._http.post(this.url + "/order/new", params, { headers: headers }).map(function (res) { return res.json(); });
    };
    OrdersService.prototype.getOrder = function () {
        return this._http.get(this.url + "/order").map(function (res) { return res.json(); });
    };
    OrdersService.prototype.getAll = function () {
        return this._http.get(this.url + "/order/getall").map(function (res) { return res.json(); });
    };
    OrdersService.prototype.getSupplier = function () {
        return this._http.get(this.url + "/supplier").map(function (res) { return res.json(); });
    };
    OrdersService.prototype.getRawmatter = function () {
        return this._http.get(this.url + "/rawmatter").map(function (res) { return res.json(); });
    };
    OrdersService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], OrdersService);
    return OrdersService;
}());
exports.OrdersService = OrdersService;
//# sourceMappingURL=orders.service.js.map