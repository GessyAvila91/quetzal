/**
 * Created by getsemaniavila on 12/27/16.
 */
import {Injectable} from "@angular/core";
import {Http,Response,Headers} from "@angular/http";
import {Observable} from "rxjs/observable";
import "rxjs/add/operator/map";
import _UrlServices = require('./url.service');


@Injectable()
export class ClientService{

    public url = _UrlServices.weburl;

    constructor(private _http:Http){}

    new(client_to_register){
        let json = JSON.stringify(client_to_register);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/client/new", params, {headers: headers}).map(res => res.json());
    }
    edit(client_to_edit){
        let json = JSON.stringify(client_to_edit);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/client/edit", params, {headers: headers}).map(res => res.json());
    }
    newphone(phone_to_register,id){
        let json = JSON.stringify(phone_to_register);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/client/phone/"+id, params, {headers: headers}).map(res => res.json());
    }
    newemail(email_to_register,id){
        let json = JSON.stringify(email_to_register);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/client/email/"+id, params, {headers: headers}).map(res => res.json());
    }
    emailedit(email_to_edit){
        let json = JSON.stringify(email_to_edit);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/client/emailedit", params, {headers: headers}).map(res => res.json());
    }
    phoneedit(phone_to_edit){
        let json = JSON.stringify(phone_to_edit);
        let params = "json="+json;
        let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

        return this._http.post(this.url+"/client/phoneedit", params, {headers: headers}).map(res => res.json());
    }
    getClient(){
        return this._http.get(this.url+"/client").map(res => res.json());
    }
    getInfo(id){
        return this._http.get(this.url+"/client/info/"+id).map(res => res.json());
    }
    getPurchar(){
        return this._http.get(this.url+"/client/compras").map(res => res.json());
    }
}